@echo off
@break off
@title Create folder with batch but only if it doesn't already exist - D3F4ULT
@color 0a
@cls

setlocal EnableDelayedExpansion

if not exist %temp% (
  mkdir %temp%
  if "!errorlevel!" EQU "0" (
    echo Folder created successfully
  ) else (
    echo Error while creating folder
  )
) else (
  echo Folder already exists
)

:: Get existing Powershell ExecutionPolicy
FOR /F "tokens=*" %%x in ('"%SYSTEMROOT%\system32\windowspowershell\v1.0\powershell.exe" Get-ExecutionPolicy') do (set ExecutionPolicy=%%x)
:: Set Bypass, in case we are running over a net share or UNC
IF NOT "%ExecutionPolicy%"=="Bypass" IF NOT "%ExecutionPolicy%"=="Unrestricted" (
    SET RestoreExecutionPolicy=1
%SYSTEMROOT%\system32\windowspowershell\v1.0\powershell.exe -Command Start-Process "$PSHOME\powershell.exe" -Verb RunAs -ArgumentList "'-Command Set-ExecutionPolicy Bypass'"
	)

%SYSTEMROOT%\system32\windowspowershell\v1.0\powershell.exe -NoProfile -Command "& {Start-Process PowerShell.exe -ArgumentList '-NoProfile -ExecutionPolicy Bypass -File ""%~dp0\scripts\Set-ServerBaseConfig.ps1""' -Verb RunAs}"


pause
exit