﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2015 v4.2.81
	 Created on:   	21/05/2015 17:00
	 Created by:   	Alessio Vigilante alessio.vigilante@techroad.it
	 Organization: 	TechRoad
	 Filename:     	Set-ObjectCacheAccount.ps1
	===========================================================================
	.DESCRIPTION
#>

#region Function
#region Utility
function FormatString($StringValue, $MaxLength, $Left, $FillChar)
{
	if ($StringValue -eq $null)
	{ $StringValue = "" }
	
	if ($StringValue.Length -gt $MaxLength)
	{ $StringValue = $StringValue.Substring(0, $MaxLength) }
	
	if ($Left)
	{ return $StringValue.PadLeft($MaxLength, $FillChar) }
	else
	{ return $StringValue.PadRight($MaxLength, $FillChar) }
}
#endregion
#region SubMenuWebApplication
function SubMenuWebApplication($Action)
{
	do
	{
		$Exit = "0"
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
		Write-Host "Select one of the available options:"
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
		$wapps = Get-SPWebApplication -IncludeCentralAdministration
		$i = 1
		
		foreach ($wa in $wapps)
		{
			Write-Host "$i)"$wa.Url
			$i += 1
		}
		Write-Host "a) All Web Application"
		Write-Host "b) Insert Web App Identity"
		Write-Host "0) Exit"
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
		$OpChoiceSubMenu = Read-Host
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
		
		
		if ($OpChoiceSubMenu -eq "0")
		{
			$Exit = "1"
			continue
		}
		elseif ($OpChoiceSubMenu -eq "a")
		{
			Write-Host "Setting All Web Apps, are you sure? (Y/N)"
			$OpConfirm = Read-Host
			if (($OpConfirm -eq "Y") -and ($OpConfirm -eq "y"))
			{
				foreach ($wa in $wapps)
				{
					Write-Host $wa.Url
					if ($Action -eq "1")
					{
						SetCacheUser -WebApplicationUrl $wa.Url
					}
					elseif ($Action -eq "2")
					{
						RemoveCacheUser -WebApplicationUrl $wa.Url
					}
				}
				Write-Host -ForegroundColor Green "Done!"
			}
		}
		elseif ($OpChoiceSubMenu -eq "b")
		{
			Write-Host "Please, insert web app identity:"
			$WebAppUrl = Read-Host
			Write-Host "Setting $WebAppUrl, are you sure? (Y/N)"
			$OpConfirm = Read-Host
			if (($OpConfirm -eq "Y") -and ($OpConfirm -eq "y"))
			{
				foreach ($wa in $wapps)
				{
					Write-Host $WebAppUrl
					if ($Action -eq "1")
					{
						SetCacheUser -WebApplicationUrl $WebAppUrl
					}
					elseif ($Action -eq "2")
					{
						RemoveCacheUser -WebApplicationUrl $WebAppUrl
					}
				}
				Write-Host -ForegroundColor Green "Done!"
			}
		}
		else
		{
			Write-Host "Setting"$wapps[$OpChoiceSubMenu-1].Url" are you sure? (Y/N)"
			$OpConfirm = Read-Host
			if (($OpConfirm -ne "Y") -and ($OpConfirm -ne "y"))
			{
				continue
			}
			if ($Action -eq "1")
			{
				SetCacheUser -WebApplicationUrl $wapps[$OpChoiceSubMenu-1].Url
			}
			elseif ($Action -eq "2")
			{
				RemoveCacheUser -WebApplicationUrl $wapps[$OpChoiceSubMenu-1].Url
			}
			Write-Host -ForegroundColor Green "Done!"
		}
		
	}
	while ($Exit -eq "0")
}
#endregion

#region Procedure
function SetCacheUser($WebApplicationUrl)
{
	$web = Get-SPWebApplication $WebApplicationUrl
	
	$policy = $web.Policies.Add($userOrGroup, "Super User Account")
	$policyRole = $web.PolicyRoles.GetSpecialRole([Microsoft.SharePoint.Administration.SPPolicyRoleType]::FullControl)
	$policy.PolicyRoleBindings.Add($policyRole)	
	$policy = $web.Policies.Add($userOrGroup, "Super Reader Account")
	$policyRole = $web.PolicyRoles.GetSpecialRole([Microsoft.SharePoint.Administration.SPPolicyRoleType]::FullRead)
	$policy.PolicyRoleBindings.Add($policyRole)
	
	if ($web.UseClaimsAuthentication -eq "True")
	{		
		$web.Properties["portalsuperuseraccount"] = "i:0#.w|eninet\adminspsuperuser"
		$web.Properties["portalsuperreaderaccount"] = "i:0#.w|eninet\adminspsuperreader"
		$web.Properties["portalsuperuseraccount"]
		$web.Properties["portalsuperreaderaccount"]
		$web.Update()
	}
	else
	{
		$web.Properties["portalsuperuseraccount"] = "eninet\adminspsuperuser"
		$web.Properties["portalsuperreaderaccount"] = "eninet\adminspsuperreader"
		$web.Properties["portalsuperuseraccount"]
		$web.Properties["portalsuperreaderaccount"]
		$web.Update()
	}
}
function RemoveCacheUser($WebApplicationUrl)
{
	$web = Get-SPWebApplication $WebApplicationUrl
	$web.Properties.Remove("portalsuperuseraccount")
	$web.Properties.Remove("portalsuperreaderaccount")
	Write-Host -ForegroundColor 'Magenta' "Please, perform a farm IISRESET"
}
#endregion
#endregion

function Main($ScriptPath)
{
	if ((Get-PSSnapin | Where { $_.Name -eq "Microsoft.SharePoint.PowerShell" }) -eq $null)
	{
		Add-PSSnapin Microsoft.SharePoint.PowerShell;
	}
	do
	{
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
		Write-Host "Select one of the available options:"
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
		Write-Host "1) Set Object Cache Super User"
		Write-Host "2) Remove Object Cache Super User"
		Write-Host "0) Exit"
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
		$OpChoice = Read-Host
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
		
		if (($OpChoice -eq "1") -or ($OpChoice -eq "2"))
		{
			Write-Host "Are you sure? (Y/N)"
			$OpConfirm = Read-Host
			if (($OpConfirm -ne "Y") -and ($OpConfirm -ne "y"))
			{
				continue
			}
			Write-Host -ForegroundColor Yellow "Starting Operation..."
			SubMenuWebApplication -Action $OpChoice
			Write-Host -ForegroundColor Green "Operation End";
		}		
	}
	while ($OpChoice -ne "0")
}
$ScriptPath = $myinvocation.mycommand.path | Split-Path
main -ScriptPath $ScriptPath