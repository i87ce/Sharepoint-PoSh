﻿param(
    [string][Parameter(Mandatory=$True)]$userLogin, 
    [string][Parameter(Mandatory=$True)]$mysiteurl, 
    [string][Parameter(Mandatory=$True)]$saveLocation)
# SharePoint DLL
Add-PSSnapin Microsoft.SharePoint.PowerShell

$mysite = Get-SPSite $mysiteurl
$context = [Microsoft.SharePoint.SPServiceContext]::GetContext($mysite)
$upm =  New-Object Microsoft.Office.Server.UserProfiles.UserProfileManager($context, $true)

Write-Host "User Profile Manager"
$exists = $upm.UserExists($userlogin)

if ($exists -eq $true)
{
    Write-Host "User Exists" -nonewline 
    Write-Host "Saving properties values"
    $up = $upm.GetUserProfile($userlogin)
    $feedIdentifier = $up["SPS-FeedIdentifier"]
    $kne = $up["AboutMe"].Value
    $soge = $up["SecurityInOilAndGasEnvironment"].Value
    $tags = $up["SPS-HashTags"].Value
    $displayName = $up["PreferredName"].Value
    $disclaimerAccepted = $up["DisclaimerAccepted"].Value
    $disclaimerAcceptedDate = $up["DisclaimerAcceptedDate"].Value
    $kmsRole = $up["KMSRole"].Value
    $PictureURL = $up["PictureURL"].Value
    $MailDigestOptions = $up["MailDigestOptions"].Value

    if($kne -eq $null) {
        $kne = "   "
    }

    $savePath = [System.IO.Path]::Combine($saveLocation,"feedIdentifier.txt")
    $feedIdentifier > $savePath

    Write-Host "Deleting User Profile"
    $upm.RemoveUserProfile($userlogin)
}

Write-Host "Re-Creating User Profile"
$up = $upm.CreateUserProfile($userlogin)

Write-Host "Restoring saved fields"
$up["SPS-FeedIdentifier"].Value = $feedIdentifier
$up["PreferredName"].Value = $displayName
$up["AboutMe"].Value = $kne
$up["SecurityInOilAndGasEnvironment"].Value = $soge
$up["SPS-HashTags"].Value = $tags
$up["DisclaimerAccepted"].Value = $disclaimerAccepted
$up["DisclaimerAcceptedDate"].Value = $disclaimerAcceptedDate
$up["KMSRole"].Value = $kmsRole
$up["PictureURL"].Value = $PictureURL 
$up["MailDigestOptions"].Value = $MailDigestOptions 
$up.Commit()