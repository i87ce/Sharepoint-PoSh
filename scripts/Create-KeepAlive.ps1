Import-Module WebAdministration

Function Remove-InvalidFileNameChars {
  param(
    [Parameter(Mandatory=$true,
      Position=0,
      ValueFromPipeline=$true,
      ValueFromPipelineByPropertyName=$true)]
    [String]$Name
  )

  $invalidChars = [IO.Path]::GetInvalidFileNameChars() -join ''
  $re = "[{0}]" -f [RegEx]::Escape($invalidChars)
  return ($Name -replace $re)
}

Function Main
{
	$HtdocsDir = Get-ItemProperty HKLM:\SOFTWARE\Microsoft\InetStp -Name PathWWWRoot
	$HtdocsDir = $HtdocsDir.PathWWWRoot

	$FolderExist = test-path -path $HtdocsDir\public
	if (!$FolderExist)
	{
		Write-Warning "Creating directory ${HtdocsDir}\public .."
		New-Item -type directory -path $HtdocsDir\public | Out-Null
	}

	$countWebSites = (Get-ChildItem IIS:\Sites | Measure-Object).count
	if ($countWebSites -gt 0) 
	{
	  $IISSites = Get-ChildItem IIS:\Sites
	  
	  foreach ($IISSite in $IISSites)
	  {
		$SiteName = $IISSite.Name
		
		Write-Host "Working on $SiteName"
		
		$CheckPublic = (Get-ChildItem IIS:\Sites\$SiteName | ? {$_.Name -eq "public"}).Count
		
		if (!($CheckPublic -gt 0))
		{
			Write-Warning "Creating Virtual Directory ..."
			$KASite = New-WebVirtualDirectory -Site $IISSite.Name -Name "Public" -PhysicalPath "${HtdocsDir}\public"
			ConvertTo-WebApplication -PSPath $KASite.PSPath  | Out-Null
			
			$authentications = Get-WebConfiguration -filter "system.webServer/security/authentication/*" -PSPath $KASite.PSPath
			
			foreach ($auth in $authentications)
			{
				$matches = $auth.SectionPath -match "/anonymousAuthentication$"		
				
				Set-WebConfigurationProperty -filter $auth.SectionPath -name enabled -value $matches -PSPath "IIS:\" -location "${SiteName}/public" | Out-Null
			}
		}
		else
		{
			Write-host "Virtual Directory Already Exist"
		}

		foreach ($Binding in $IISSite.bindings.Collection)
		{
			$KAFileName = $Binding.bindingInformation
			$KAFileName = $KAFileName.Split(":")
			$KAFileName = $KAFileName[-1] + "_" + $KAFileName[-2] + ".html"
		
		
			$KAFileName = Remove-InvalidFileNameChars -Name $KAFileName
		
			$KAExist = test-path -path "${HtdocsDir}\public\${KAFileName}"
			
			if (!$KAExist)
			{
				$ServerName = "Alive " + $env:computername
				[System.IO.File]::WriteAllText("${HtdocsDir}\public\${KAFileName}", $ServerName) | Out-Null
			}
		}
		
		
		
	  }
	}
}
Main
Read-Host "Done..."