Param([string]$DatabaseName,
        [string]$DatabaseServer,
        [switch]$IncludeFeatures,
        [switch]$IncludeEventReceivers)

############################################################
#SP_Enum-AllWebs_vHSG.ps1
#
#Author: Brian T. Jackett
#Last Modified Date: 2013-11-08
#
#Script to replace functionality from STSADM -o enumallwebs
############################################################

# some references can be used from the below article
#http://www.craigtothepoint.com/Lists/Posts/Post.aspx?ID=11

function ProcessContentDatabase([Microsoft.SharePoint.Administration.SPContentDatabase]$SPContentDatabase)
{
        Write-Verbose "Begin - Processing content database: $($db.Name)"
        Write-Output "<Database SiteCount=`"$($db.CurrentSiteCount)`" Name=`"$($db.Name)`" DataSource=`"$($db.Server)`">"

        foreach($site in $db.Sites)
        {
            ProcessSiteCollection $site
        }
        Write-Output "</Database>"
        Write-Verbose "End - Processing content database: $($db.Name)"
}

function ProcessSiteCollection([Microsoft.SharePoint.SPSite]$SPSite)
{
    Write-Verbose "Begin - Processing site collection: $($site.URL)"

    if($site.HostHeaderIsSiteName -eq $false)
    {
        Write-Output "<Site Id=`"$($site.Id)`" OwnerLogin=`"$($site.Owner.UserLogin)`" InSiteMap=`"$($configDB.SiteExists($($site.Id)))`">"
    }
    else
    {
        Write-Output "<Site Id=`"$($site.Id)`" OwnerLogin=`"$($site.Owner.UserLogin)`" InSiteMap=`"$($configDB.SiteExists($($site.Id)))`" HostHeader=`"$($site.HostName)`">"
    }

    if($IncludeEventReceivers)
    {
        Write-Output "<EventReceiverAssemblies>"
        foreach($eventReceiver in $site.EventReceivers)
        {
            Write-Output "<EventReceiverAssembly Name=`"$($eventReceiver.Assembly)`" />"
        }
        Write-Output "</EventReceiverAssemblies>"
    }
    if($IncludeFeatures)
    {
        Write-Output "<Features>"
        foreach($feature in $site.Features)
        {
            Write-Output "<Feature Id=`"$($feature.DefinitionId)`" Count=`"1`" DisplayName=`"$($feature.Definition.DisplayName)`" InstallPath=`"$($feature.Definition.RootDirectory)`" Status=`"$($feature.Definition.Status)`" />"
        }
        Write-Output "</Features>"
    }

    Write-Output "<Webs Count=`"$($site.AllWebs.Count)`">"

    foreach($web in $site.AllWebs)
    {
        ProcessSite $web
    }
    Write-Output "</Webs>"

    Write-Output "</Site>"
    Write-Verbose "End - Processing site collection: $($site.URL)"

    $site.Dispose()
}

function ProcessSite([Microsoft.SharePoint.SPWeb]$SPWeb)
{
    Write-Verbose "Begin - Processing site: $($web.URL)"

    if(!($IncludeFeatures -or $IncludeEventReceivers))
    {
        Write-Output "<Web Id=`"$($web.Id)`" Url=`"$($web.Url)`" Languauge=`"$($web.Language)`" TemplateName=`"$($web.WebTemplate)#$($web.Configuration)`" TemplateId=`"$($web.WebTemplateId)`" />"
    }
    else
    {
        Write-Output "<Web Id=`"$($web.Id)`" Url=`"$($web.Url)`" Languauge=`"$($web.Language)`" TemplateName=`"$($web.WebTemplate)#$($web.Configuration)`" TemplateId=`"$($web.WebTemplateId)`">"

        if($IncludeEventReceivers)
        {
            Write-Output "<EventReceiverAssemblies>"
            foreach($eventReceiver in $web.EventReceivers)
            {
                Write-Output "<EventReceiverAssembly Name=`"$($eventReceiver.Assembly)`" />"
            }
            Write-Output "</EventReceiverAssemblies>"
        }
        if($IncludeFeatures)
        {
            Write-Output "<Features>"
            foreach($feature in $web.Features)
            {
                Write-Output "<Feature Id=`"$($feature.DefinitionId)`" Count=`"1`" DisplayName=`"$($feature.Definition.DisplayName)`" InstallPath=`"$($feature.Definition.RootDirectory)`" Status=`"$($feature.Definition.Status)`" />"
            }
            Write-Output "</Features>"
        }
        Write-Output "</Web>"
    }
    Write-Verbose "End - Processing site: $($web.URL)"

    $web.Dispose()
}


# start of main portion of script
if((Get-PSSnapin -Name Microsoft.SharePoint.PowerShell) -eq $null)
{
    Add-PSSnapin Microsoft.SharePoint.PowerShell
}

$farm = [Microsoft.SharePoint.Administration.SPFarm]::Local

# check if farm was able to be referenced
if($farm -eq $null)
{
    Write-Error "Unable to access farm, exiting script"
    exit
}
else
{
    # get reference to configuration database to be used in site collection processing
    $configDB = Get-SPDatabase | where {$_.typename -eq "Configuration Database"}
    [Microsoft.SharePoint.Administration.SPContentDatabase[]]$contentDB = Get-SPWebApplication -IncludeCentralAdministration | Select-Object -ExpandProperty ContentDatabases

    # filter content databases based on user input (if supplied)
    if(-not [string]::IsNullOrEmpty($DatabaseName))
    {
        $contentDB = $contentDB | Where-Object {$_.Name -eq $DatabaseName}
    }
    if(-not [string]::IsNullOrEmpty($DatabaseServer))
    {
        $contentDB = $contentDB | Where-Object {$_.Server -eq $DatabaseServer}
    }

    Write-Output "<Databases>"
    foreach($db in $contentDB)
    {
        ProcessContentDatabase $db

    }
    Write-Output "</Databases>"
}