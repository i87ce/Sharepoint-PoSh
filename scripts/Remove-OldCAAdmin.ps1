#################################################
# Add a new Farm Administrator
#################################################
Add-PSSnapin *SharePoint* -erroraction SilentlyContinue


function RemoveWebAppUser($WebApplicationUrl, $userOrGroup)
{
	
	$web = Get-SPWebApplication $WebApplicationUrl
	write-host "Remove user $userOrGroup" -fore cyan 
	$policy = $web.Policies.Remove($userOrGroup)
	$web.Update()
} 

Write-Host
$listaserver = get-spserver | ? { $_.Role -eq "Application" }

$old1 = "eni\gg.profiles.eni-epts_webapp.eni"
$old1Domain = ($old1 -split "\\")[0]
$old1Account = ($old1 -split "\\")[1]

$old2 = "eninet\gg.profiles.eni-epts_webapp.eninet"
$old2Domain = ($old2 -split "\\")[0]
$old2Account = ($old2 -split "\\")[1]

Write-Host




	$adminwebapp = Get-SPwebapplication -includecentraladministration | where {$_.IsAdministrationWebApplication}  
	$adminsite = Get-SPweb($adminwebapp.Url)  
	$admingroup = $adminsite.AssociatedOwnerGroup  
	
	Write-Host "Remove Old Admin"
	write-host "Remove user $old1 from the SP farm admin group" -fore cyan 
	$user1 = get-spuser $old1 -web $adminwebapp.Url
	$adminsite.SiteGroups[$admingroup].RemoveUser($user1) 
	write-host "User $old1 removed successfully from the SP farm admin group" -fore green 
	write-host "Remove user $old2 from the SP farm admin group" -fore cyan 
	$user2 = get-spuser $old2 -web $adminwebapp.Url
	$adminsite.SiteGroups[$admingroup].RemoveUser($user2)  
	write-host "User $old2 removed successfully from the SP farm admin group" -fore green 

	Write-Host "Continue as Local Admin"
	
	$group = "Administrators"
	foreach ($server in $listaserver)
	{
		$pc = $server.Address
		
		Write-Host "Rimuovo User: "$old1Domain"\"$old1Account" al server: "$pc
		$objUser = [ADSI]("WinNT://$old1Domain/$old1Account")
		$objGroup = [ADSI]("WinNT://$pc/$group")
		$objGroup.PSBase.Invoke("Remove",$objUser.PSBase.Path)
		
		Write-Host "Rimuovo User: "$old2Domain"\"$old2Account" al server: "$pc
		$objUser = [ADSI]("WinNT://$old2Domain/$old2Account")
		$objGroup = [ADSI]("WinNT://$pc/$group")
		$objGroup.PSBase.Invoke("Remove",$objUser.PSBase.Path)
	}
	
	Write-Host "Continue as Web App User Policy Full Control"
	$wapps = Get-SPWebApplication
	foreach ($wa in $wapps)
	{
		Write-Host $wa.Url
		RemoveWebAppUser -WebApplicationUrl $wa.Url -userOrGroup $old1
		RemoveWebAppUser -WebApplicationUrl $wa.Url -userOrGroup "c:0+.w|s-1-5-21-220523388-1801674531-682003330-1210169"
		RemoveWebAppUser -WebApplicationUrl $wa.Url -userOrGroup $old2
		RemoveWebAppUser -WebApplicationUrl $wa.Url -userOrGroup "c:0+.w|s-1-5-21-4235238967-1270027118-691234884-32224"
	}
	
	Write-Host "Completed Succesfully!"
	Read-host