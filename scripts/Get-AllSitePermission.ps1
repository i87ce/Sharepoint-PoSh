## Input Parameters ##
[CmdletBinding()]
Param(
[string]$WebAppUrl=(Read-Host "Please enter a Web Application URL (Example: http://intranet)"),
[string]$Admin=(Read-Host "Please enter Primary Site Collection Admin (Examples: ENINET\AdminSPFarm10)")
)
## Add SharePoint Snap-in ##
Add-PSSnapin Microsoft.SharePoint.PowerShell -erroraction SilentlyContinue
### No need to modify these ###
$WebApp = Get-SPWebApplication $WebAppUrl
$AllSites = $WebApp | Get-SPSite -Limit ALL
 
############################# The Script - no need to modify this section #############################
## Start SP Assignment for proper disposal of variables and objects ##
Write-Host "Starting SP Assignment..." -ForegroundColor Green
Start-SPAssignment -Global
## Use a ForEach-Object loop and Set-SPSiteAdministration to set an entire web application ##
Write-Host "Setting $WebAppUrl Admin to $Admin..." -ForegroundColor Yellow
$AllSites | ForEach-Object { Set-SPSite -Identity $_.url -OwnerAlias $Admin }
## End SP Assignment for proper disposal of variables and objects ##
Write-Host "Disposing of SP Objects.." -ForegroundColor Green
Stop-SPAssignment -Global
## Tell us it's done ##
Write-Host "Finished!" -ForegroundColor Green
