﻿Add-PsSnapin Microsoft.SharePoint.PowerShell

$listaserver = get-spserver | ? { $_.Role -eq "Application" }
$group = "SHPLogs"
$userdomain=(Read-Host "Please enter the user domain (Example: eninet)")
$user = Read-Host "enter domain user id"

foreach ($server in $listaserver)
{
	$pc = $server.Address
	Write-Host "Aggiungo User: "$userdomain"\"$user" al server: "$pc
	$objUser = [ADSI]("WinNT://$userdomain/$user")
	$objGroup = [ADSI]("WinNT://$pc/$group")
	$objGroup.PSBase.Invoke("Add",$objUser.PSBase.Path)
}

Remove-PsSnapin Microsoft.SharePoint.PowerShell
Echo Finish




