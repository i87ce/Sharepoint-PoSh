# --------------------------------------------------------------------
# Check and Enable LinkedConnections
# --------------------------------------------------------------------
function Enable-LinkedConnections
{
    $ReturnCode = 0
    Try
    {
        Write-Host "Disabling UAC..."
        $UACPath = "HKLM:\Software\Microsoft\Windows\CurrentVersion\policies\system"
        $UACPathValue = Get-ItemProperty -path $UACPath
        If (-not ($UACPathValue.EnableLinkedConnections -eq "1"))
        {
            Set-ItemProperty HKLM:\Software\Microsoft\Windows\CurrentVersion\policies\system -Name "EnableLinkedConnections" -Value "1" -PropertyType dword
            Write-Host "...Disabled!"
            $ReturnCode = 0
        }
        else
        {
            Write-Host "...Already Disabled!"
            $ReturnCode = 0
        }
    }
    Catch
	{
		$ReturnCode = -1
		Write-Warning "An error has occurred when checking yor registry"
		Write-Error $_
		break
	}
}

# --------------------------------------------------------------------
# Check and Disable UAC
# --------------------------------------------------------------------
Function DisableUAC 
{
    $ReturnCode = 0
    Try
    {
        Write-Host "Disabling UAC..."
        $UACPath = "HKLM:\Software\Microsoft\Windows\CurrentVersion\policies\system"
        $UACPathValue = Get-ItemProperty -path $UACPath
        If (-not ($UACPathValue.EnableLUA -eq "1"))
        {
            Set-ItemProperty HKLM:\Software\Microsoft\Windows\CurrentVersion\policies\system -Name "EnableLUA" -Value "1" -PropertyType dword
            Write-Host "...Disabled!"
            $ReturnCode = 0
        }
        else
        {
            Write-Host "...Already Disabled!"
            $ReturnCode = 0
        }
    }
    Catch
	{
		$ReturnCode = -1
		Write-Warning "An error has occurred when checking yor registry"
		Write-Error $_
		break
	}
}

# --------------------------------------------------------------------
# Disable Windows Firewall
# --------------------------------------------------------------------
function Disable-Firewall
{
	netsh advfirewall set Domainprofile state off
	netsh advfirewall set Privateprofile state off
	netsh advfirewall set Publicprofile state off
}

# --------------------------------------------------------------------
# Disable IE Esc
# --------------------------------------------------------------------
Function Disable-IEESC
{

        Write-Host -ForegroundColor White " - Disabling IE Enhanced Security..."
        Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A7-37EF-4b3f-8CFC-4F3A74704073}" -Name isinstalled -Value 0
        Set-ItemProperty -Path "HKLM:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A8-37EF-4b3f-8CFC-4F3A74704073}" -Name isinstalled -Value 0
        Rundll32 iesetup.dll, IEHardenLMSettings,1,True
        Rundll32 iesetup.dll, IEHardenUser,1,True
        Rundll32 iesetup.dll, IEHardenAdmin,1,True
        If (Test-Path "HKCU:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A7-37EF-4b3f-8CFC-4F3A74704073}")
        {
            Remove-Item -Path "HKCU:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A7-37EF-4b3f-8CFC-4F3A74704073}"
        }
        If (Test-Path "HKCU:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A8-37EF-4b3f-8CFC-4F3A74704073}")
        {
            Remove-Item -Path "HKCU:\SOFTWARE\Microsoft\Active Setup\Installed Components\{A509B1A8-37EF-4b3f-8CFC-4F3A74704073}"
        }

        #This doesn't always exist
        Remove-ItemProperty "HKCU:\SOFTWARE\Microsoft\Internet Explorer\Main" "First Home Page" -ErrorAction SilentlyContinue
}