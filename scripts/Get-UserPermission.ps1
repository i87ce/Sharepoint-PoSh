Add-PSSnapin Microsoft.SharePoint.PowerShell -ErrorAction SilentlyContinue

Function GetUserAccessReport($WebAppURL, $SiteColl, $SearchUser)
{
	#Get All Site Collections of the WebApp
	if ([string]::IsNullOrEmpty($SiteColl)) 
	{
		Write-Host $WebAppURL
		$SiteCollections = Get-SPSite -WebApplication $WebAppURL -Limit All
	}
	else
	{
		$tmp = $WebAppURL + $SiteColl
		Write-Host $tmp
		$SiteCollections = Get-SPSite -Identity $tmp
	}

	#Write CSV- TAB Separated File) Header
	"URL `t Site/List `t Title `t PermissionType `t Permissions `t LoginName `t DisplayName" | out-file UserAccessReport.csv

	#Check Whether the Search Users is a Farm Administrator
	if (![string]::IsNullOrEmpty($SearchUser))
	{
		$AdminWebApp= Get-SPwebapplication -includecentraladministration | where {$_.IsAdministrationWebApplication}
		$AdminSite = Get-SPweb($AdminWebApp.Url)
		$AdminGroupName = $AdminSite.AssociatedOwnerGroup
		$FarmAdminGroup = $AdminSite.SiteGroups[$AdminGroupName]

		foreach ($user in $FarmAdminGroup.users)
		{
			if($user.LoginName -eq $SearchUser)
			{
				"$($AdminWebApp.URL) `t Farm `t $($AdminSite.Title)`t Farm Administrator `t Farm Administrator `t $($user.LoginName) `t $($user.DisplayName)" | Out-File UserAccessReport.csv -Append
			}     
		}
	}
	
	#Check Web Application Policies
	$WebApp = Get-SPWebApplication $WebAppURL

	foreach ($Policy in $WebApp.Policies)
	{
		if (![string]::IsNullOrEmpty($SearchUser))
		{
			#Check if the search users is member of the group
			if($Policy.UserName -eq $SearchUser)
			{
				Write-Host $Policy.UserName -ForegroundColor "DarkBlue"
				$PolicyRoles=@()
				foreach($Role in $Policy.PolicyRoleBindings)
				{
					$PolicyRoles+= $Role.Name +";"
				}
				Write-Host "Permissions: " $PolicyRoles -ForegroundColor "DarkBlue"

				"$($AdminWebApp.URL) `t Web Application `t $($AdminSite.Title)`t  Web Application Policy `t $($PolicyRoles) `t $($Policy.UserName) `t $($Policy.DisplayName)" | Out-File UserAccessReport.csv -Append
			}
		}
		else
		{
			Write-Host $Policy.UserName -ForegroundColor "DarkBlue"
			$PolicyRoles=@()
			foreach($Role in $Policy.PolicyRoleBindings)
			{
				$PolicyRoles+= $Role.Name +";"
			}
			Write-Host "Permissions: " $PolicyRoles -ForegroundColor "DarkBlue"

			"$($AdminWebApp.URL) `t Web Application `t $($AdminSite.Title)`t  Web Application Policy `t $($PolicyRoles) `t $($Policy.UserName) `t $($Policy.DisplayName)" | Out-File UserAccessReport.csv -Append			
		}
	}

	#Loop through all site collections
	foreach($Site in $SiteCollections)
	{
		#Check Whether the Search User is a Site Collection Administrator
		foreach($SiteCollAdmin in $Site.RootWeb.SiteAdministrators)
		{
			if (![string]::IsNullOrEmpty($SearchUser))
			{
				if($SiteCollAdmin.LoginName -eq $SearchUser)
				{
					"$($Site.RootWeb.Url) `t Site `t $($Site.RootWeb.Title)`t Site Collection Administrator `t Site Collection Administrator `t $($SiteCollAdmin.LoginName) `t $($SiteCollAdmin.DisplayName)" | Out-File UserAccessReport.csv -Append
				}    
			}	
			else
			{
				"$($Site.RootWeb.Url) `t Site `t $($Site.RootWeb.Title)`t Site Collection Administrator `t Site Collection Administrator `t $($SiteCollAdmin.LoginName) `t $($SiteCollAdmin.DisplayName)" | Out-File UserAccessReport.csv -Append
			}
		}

		#Loop throuh all Sub Sites
		foreach($Web in $Site.AllWebs)
		{
			if($Web.HasUniqueRoleAssignments -eq $True)
			{
				#Get all the users granted permissions to the list
				foreach($WebRoleAssignment in $Web.RoleAssignments )
				{
					#Is it a User Account?
					if($WebRoleAssignment.Member.userlogin)   
					{
						if (![string]::IsNullOrEmpty($SearchUser))
						{
							#Is the current user is the user we search for?
							if($WebRoleAssignment.Member.LoginName -eq $SearchUser)
							{
								Write-Host  $SearchUser has direct permissions to site $Web.Url -ForegroundColor "Blue"
								#Get the Permissions assigned to user
								$WebUserPermissions=@()
								foreach ($RoleDefinition  in $WebRoleAssignment.RoleDefinitionBindings)
								{
									$WebUserPermissions += $RoleDefinition.Name +";"
								}
								write-host "with these permissions: " $WebUserPermissions -ForegroundColor "Blue"
								#Send the Data to Log file
								"$($Web.Url) `t Site `t $($Web.Title)`t Direct Permission `t $($WebUserPermissions) `t $($WebRoleAssignment.Member.LoginName) `t $($WebRoleAssignment.Member.DisplayName)" | Out-File UserAccessReport.csv -Append
							}
						}
						else
						{
								Write-Host  $($WebRoleAssignment.Member.LoginName) has direct permissions to site $Web.Url -ForegroundColor "Blue"
								#Get the Permissions assigned to user
								$WebUserPermissions=@()
								foreach ($RoleDefinition  in $WebRoleAssignment.RoleDefinitionBindings)
								{
									$WebUserPermissions += $RoleDefinition.Name +";"
								}
								write-host "with these permissions: " $WebUserPermissions -ForegroundColor "Blue"
								#Send the Data to Log file
								"$($Web.Url) `t Site `t $($Web.Title)`t Direct Permission `t $($WebUserPermissions) `t $($WebRoleAssignment.Member.LoginName) `t $($WebRoleAssignment.Member.DisplayName)" | Out-File UserAccessReport.csv -Append							
						}
					}
					#Its a SharePoint Group, So search inside the group and check if the user is member of that group
					else 
					{
						foreach($user in $WebRoleAssignment.member.users)
						{
							if (![string]::IsNullOrEmpty($SearchUser))
							{
								#Check if the search users is member of the group
								if($user.LoginName -eq $SearchUser)
								{
									Write-Host  "$SearchUser is Member of " $WebRoleAssignment.Member.Name "Group" -ForegroundColor "Blue"
									#Get the Group's Permissions on site
									$WebGroupPermissions=@()
									foreach ($RoleDefinition  in $WebRoleAssignment.RoleDefinitionBindings)
									{
										$WebGroupPermissions += $RoleDefinition.Name +";"
									}
									write-host "Group has these permissions: " $WebGroupPermissions -ForegroundColor "Blue"

									#Send the Data to Log file
									"$($Web.Url) `t Site `t $($Web.Title)`t Member of $($WebRoleAssignment.Member.Name) Group `t $($WebGroupPermissions) `t $($user.LoginName) `t $($user.DisplayName)" | Out-File UserAccessReport.csv -Append
								}
							}
							else
							{
								Write-Host  "$($user.LoginName) is Member of " $WebRoleAssignment.Member.Name "Group" -ForegroundColor "Blue"
								#Get the Group's Permissions on site
								$WebGroupPermissions=@()
								foreach ($RoleDefinition  in $WebRoleAssignment.RoleDefinitionBindings)
								{
									$WebGroupPermissions += $RoleDefinition.Name +";"
								}
								write-host "Group has these permissions: " $WebGroupPermissions -ForegroundColor "Blue"

								#Send the Data to Log file
								"$($Web.Url) `t Site `t $($Web.Title)`t Member of $($WebRoleAssignment.Member.Name) Group `t $($WebGroupPermissions) `t $($user.LoginName) `t $($user.DisplayName)" | Out-File UserAccessReport.csv -Append								
							}
						}
					}
				}
			}

			#********  Check Lists with Unique Permissions ********/
			foreach($List in $Web.lists)
			{
				if($List.HasUniqueRoleAssignments -eq $True -and ($List.Hidden -eq $false))
				{
					#Get all the users granted permissions to the list
					foreach($ListRoleAssignment in $List.RoleAssignments )
					{
						#Is it a User Account?
						if($ListRoleAssignment.Member.userlogin)   
						{
							if (![string]::IsNullOrEmpty($SearchUser))
							{
								#Is the current user is the user we search for?
								if($ListRoleAssignment.Member.LoginName -eq $SearchUser)
								{
									Write-Host  $SearchUser has direct permissions to List ($List.ParentWeb.Url)/($List.RootFolder.Url) -ForegroundColor "Cyan"
									#Get the Permissions assigned to user
									$ListUserPermissions=@()
									foreach ($RoleDefinition  in $ListRoleAssignment.RoleDefinitionBindings)
									{
										$ListUserPermissions += $RoleDefinition.Name +";"
									}
									write-host "with these permissions: " $ListUserPermissions -ForegroundColor "Cyan"

									#Send the Data to Log file
									"$($List.ParentWeb.Url)/$($List.RootFolder.Url) `t List `t $($List.Title)`t Direct Permissions `t $($ListUserPermissions) `t $($ListRoleAssignment.Member.LoginName) `t $($ListRoleAssignment.Member.DisplayName)" | Out-File UserAccessReport.csv -Append
								}
							}
							else
							{
								Write-Host  $($ListRoleAssignment.Member.LoginName) has direct permissions to List ($List.ParentWeb.Url)/($List.RootFolder.Url) -ForegroundColor "Cyan"
								#Get the Permissions assigned to user
								$ListUserPermissions=@()
								foreach ($RoleDefinition  in $ListRoleAssignment.RoleDefinitionBindings)
								{
									$ListUserPermissions += $RoleDefinition.Name +";"
								}
								write-host "with these permissions: " $ListUserPermissions -ForegroundColor "Cyan"

								#Send the Data to Log file
								"$($List.ParentWeb.Url)/$($List.RootFolder.Url) `t List `t $($List.Title)`t Direct Permissions `t $($ListUserPermissions) `t $($ListRoleAssignment.Member.LoginName) `t $($ListRoleAssignment.Member.DisplayName)" | Out-File UserAccessReport.csv -Append
							}
						}
						#Its a SharePoint Group, So search inside the group and check if the user is member of that group
						else 
						{
							foreach($user in $ListRoleAssignment.member.users)
							{
								if (![string]::IsNullOrEmpty($SearchUser))
								{
									if($user.LoginName -eq $SearchUser)
									{
										Write-Host  "$SearchUser is Member of " $ListRoleAssignment.Member.Name "Group" -ForegroundColor "Cyan"
										#Get the Group's Permissions on site
										$ListGroupPermissions=@()
										foreach ($RoleDefinition  in $ListRoleAssignment.RoleDefinitionBindings)
										{
											$ListGroupPermissions += $RoleDefinition.Name +";"
										}
										write-host "Group has these permissions: " $ListGroupPermissions -ForegroundColor "Cyan"

										#Send the Data to Log file
										"$($Web.Url) `t Site `t $($List.Title)`t Member of $($ListRoleAssignment.Member.Name) Group `t $($ListGroupPermissions) `t $($user.LoginName) `t $($user.DisplayName)" | Out-File UserAccessReport.csv -Append
									}
								}
								else
								{
									Write-Host  "$($user.LoginName) is Member of " $ListRoleAssignment.Member.Name "Group" -ForegroundColor "Cyan"
									#Get the Group's Permissions on site
									$ListGroupPermissions=@()
									foreach ($RoleDefinition  in $ListRoleAssignment.RoleDefinitionBindings)
									{
										$ListGroupPermissions += $RoleDefinition.Name +";"
									}
									write-host "Group has these permissions: " $ListGroupPermissions -ForegroundColor "Cyan"

									#Send the Data to Log file
									"$($Web.Url) `t Site `t $($List.Title)`t Member of $($ListRoleAssignment.Member.Name) Group `t $($ListGroupPermissions) `t $($user.LoginName) `t $($user.DisplayName)" | Out-File UserAccessReport.csv -Append
								}
							}
						}
					}
				}
			}
		}
	}
}

#Call the function to Check User Access
$WAUrl = Read-Host "Please, insert Web Application Full Url (http://eptsu.eni.com)"
$SCPath = Read-Host "if you want to filter to a determinate site collection, insert path (/sites/constr) else leave empty"
$User = Read-Host "Please, insert full domain\user (eni\co24668) or leave blank for all users extraction"
GetUserAccessReport $WAUrl $SCPath $User