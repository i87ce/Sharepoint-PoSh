if ((Get-PSSnapin | Where { $_.Name -eq "Microsoft.SharePoint.PowerShell" }) -eq $null)
	{
		Add-PSSnapin Microsoft.SharePoint.PowerShell;
	}

$searchapp = Get-SPEnterpriseSearchServiceApplication | Where-Object {$_.name -like "Search Service"} 
$contentsource = Get-SPEnterpriseSearchCrawlContentSource -SearchApplication $searchapp  | Where-Object {$_.name -like "eCampus*"}
$contentsource.StartFullCrawl()