﻿#----- define parameters -----#
$today = (Get-Date -uformat "%d-%m-%Y_%H_%M_%S")
#----- define folder where files are located ----#
$TargetFolder = "G:\Logs\Farm\Timer Job di allineamento gruppi di utenti\"
$TargetFile = "allineamentoGruppi.log"
$LogfileIn = $TargetFolder + $TargetFile
$LogfileOut = $TargetFolder + $TargetFile.replace(".log","_$today.log")
$Filter = "allineamentoGruppi.log"
#----- get current date ----#
$Now = Get-Date
#----- define amount of days ----#
$Days = "30"
#----- define extension ----#
$Extension = "*.log"
#----- define LastWriteTime parameter based on $Days ---#
$LastWrite = $Now.AddDays(-$Days)

# Rename current log
Rename-Item $LogfileIn $LogfileOut

# New log
New-Item $LogfileIn -type file
 
#----- get files based on lastwrite filter and specified folder ---#
$Files = Get-Childitem $TargetFolder -Include $Extension -Recurse | Where {$_.LastWriteTime -le "$LastWrite"}
 
foreach ($File in $Files)
    {
    if ($File -ne $NULL)
        {
        write-host "Deleting File $File" -ForegroundColor "DarkRed"
        Remove-Item $File.FullName | out-null
        }
    else
        {
        Write-Host "No more files to delete!" -foregroundcolor "Green"
        }
    }