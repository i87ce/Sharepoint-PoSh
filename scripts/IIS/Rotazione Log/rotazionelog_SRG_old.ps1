﻿$Now = Get-Date
$today = (Get-Date -uformat "%d-%m-%Y_%H_%M_%S")

#--- Pulizia Log Prima Directory ---#
$Days = "30"
$TargetFolder = "G:\Logs\Farm\Timer Job di allineamento gruppi di utenti\"
$TargetFile = "allineamentoGruppi.log"
$LogfileIn = $TargetFolder + $TargetFile
$LogfileOut = $TargetFolder + $TargetFile.replace(".log","_$today.log")
$Filter = "allineamentoGruppi.log"

$LastWrite = $Now.AddDays(-$Days)
Rename-Item $LogfileIn $LogfileOut

$Files = Get-Childitem $TargetFolder -Include $Filter -Recurse | Where {$_.LastWriteTime -le "$LastWrite"}
foreach ($File in $Files)
    {
    Remove-Item $File.FullName | out-null
    }
New-Item $LogfileIn -type file
