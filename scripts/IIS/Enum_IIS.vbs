OPTION EXPLICIT

DIM CRLF, TAB
DIM strServer
DIM objWebService

TAB  = CHR( 9 )
CRLF = CHR( 13 ) & CHR( 10 )

IF WScript.Arguments.Length = 1 THEN
    strServer = WScript.Arguments( 0 )
ELSE
    strServer = "localhost"
END IF

WScript.Echo "Enumerating websites on " & strServer & CRLF
SET objWebService = GetObject( "IIS://" & strServer & "/W3SVC" )
EnumWebsites objWebService


SUB EnumWebsites( objWebService )
    DIM objWebServer, objWebServerRoot, strBindings
    DIM myFSO
    DIM WriteStuff
    DIM tmp 

    Set myFSO = CreateObject("Scripting.FileSystemObject")
    Set WriteStuff = myFSO.OpenTextFile(strServer + ".txt", 8, True)
    tmp = "Site ID|Comment|State|Path|LogDir|HostHeaders|SecHostHeaders"
    WriteStuff.WriteLine(tmp)


    FOR EACH objWebServer IN objWebService
        IF objWebserver.Class = "IIsWebServer" THEN
        SET objWebServerRoot = GetObject(objWebServer.adspath & "/root")
            tmp = objWebserver.Name & "|" & _
                objWebServer.ServerComment & "|" & _ 
                State2Desc( objWebserver.ServerState ) & "|" & _ 
                objWebServerRoot.path & "|" & _ 
                objWebServer.LogFileDirectory & "|" & _   
                EnumBindings(objWebServer.ServerBindings) & "|" & _
        EnumBindings(objWebServer.SecureBindings) & "|" & _
        ""
            WriteStuff.WriteLine(tmp)
        END IF
    NEXT

END SUB

FUNCTION EnumBindings( objBindingList )
    DIM i, strIP, strPort, strHost
    DIM reBinding, reMatch, reMatches
    SET reBinding = NEW RegExp
    reBinding.Pattern = "([^:]*):([^:]*):(.*)"
    EnumBindings = ""
    FOR i = LBOUND( objBindingList ) TO UBOUND( objBindingList )
        ' objBindingList( i ) is a string looking like IP:Port:Host
        SET reMatches = reBinding.Execute( objBindingList( i ) )
        FOR EACH reMatch IN reMatches
            strIP = reMatch.SubMatches( 0 )
            strPort = reMatch.SubMatches( 1 )
            strHost = reMatch.SubMatches( 2 )



            ' Do some pretty processing
            IF strIP = "" THEN strIP = "All Unassigned"
            IF strHost = "" THEN strHost = "*"
            IF LEN( strIP ) < 8 THEN strIP = strIP & TAB

        EnumBindings = EnumBindings & strHost & "," & ""
        NEXT    
    NEXT
    if len(EnumBindings) > 0 Then EnumBindings = Left(EnumBindings,Len(EnumBindings)-1)
END FUNCTION



FUNCTION State2Desc( nState )
    SELECT CASE nState
    CASE 1
        State2Desc = "Starting (MD_SERVER_STATE_STARTING)"
    CASE 2
        State2Desc = "Started (MD_SERVER_STATE_STARTED)"
    CASE 3
        State2Desc = "Stopping (MD_SERVER_STATE_STOPPING)"
    CASE 4
        State2Desc = "Stopped (MD_SERVER_STATE_STOPPED)"
    CASE 5
        State2Desc = "Pausing (MD_SERVER_STATE_PAUSING)"
    CASE 6
        State2Desc = "Paused (MD_SERVER_STATE_PAUSED)"
    CASE 7
        State2Desc = "Continuing (MD_SERVER_STATE_CONTINUING)"
    CASE ELSE
        State2Desc = "Unknown state"
    END SELECT

END FUNCTION