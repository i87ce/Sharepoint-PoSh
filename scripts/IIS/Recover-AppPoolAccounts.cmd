cls
@echo off
%WINDIR%\System32\cmd.exe /q /c %WINDIR%\system32\inetsrv\appcmd.exe list apppool > APPPOOL.TXT
echo --------------------------------------------------------------------------------
SET count=1 
FOR /F tokens^=2^ delims^=^" %%A IN ('TYPE APPPOOL.TXT') DO (
	echo Application Pool:  %%A
	echo | set /p = User: 
	%WINDIR%\System32\cmd.exe /q /c %WINDIR%\system32\inetsrv\appcmd.exe list apppool "%%A" /text:ProcessModel.UserName
    echo | set /p = Password: 
	%WINDIR%\System32\cmd.exe /q /c %WINDIR%\system32\inetsrv\appcmd.exe list apppool "%%A" /text:ProcessModel.Password 
	echo --------------------------------------------------------------------------------
	set /a count+=1 )
Del APPPOOL.TXT
Pause