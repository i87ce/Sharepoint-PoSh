@echo off
set argC=0
for %%x in (%*) do Set /A argC+=1
if %argC% == 1 (
	powershell -ExecutionPolicy Unrestricted -File .\create-instance.ps1 -i %1
) else (
	if %argC% == 2 (
		powershell -ExecutionPolicy Unrestricted -File .\create-instance.ps1 -i %1 -a %2
	) else (
		if %argC% == 3 (
			powershell -ExecutionPolicy Unrestricted -File .\create-instance.ps1 -i %1 -a %2 -p %3
		) else (
		echo Usage: %0 Instance-Name [IpAddress]
		)
	)
)