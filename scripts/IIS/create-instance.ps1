Param(
    [parameter(Mandatory=$true)]
    [alias("i")]
    $InstanceName,
    [parameter(Mandatory=$false)]
    [alias("a")]
    $IpAddress,
    [parameter(Mandatory=$false)]
    [alias("p")]
    $Port
	)

$SitesDir 		= "F:\Appl"
$AppDir 		= "${SitesDir}\${InstanceName}"
$StageAreaDir 	= "${AppDir}\StageArea"
$LogsDir 		= "${AppDir}\Logs"
$HtdocsDir 		= "${AppDir}\htdocs"

if (!$Port) {
	$Port = 80
}

function Valid-IP {
	param($Ip) 
	try {
		$ipObj = [System.Net.IPAddress]::parse($Ip)
		$isValidIP = [System.Net.IPAddress]::tryparse([string]$Ip, [ref]$ipObj)
	} catch {
		Write-Host -foreground red "L'indirizzo ip specificato non � valido"
		exit 2
	}
} 

function Exists-Drive {
	param($DriveLetter) 
	$Exists = (New-Object System.IO.DriveInfo($DriveLetter)).DriveType -ne 'NoRootDirectory'
	if (!$Exists) {
		Write-Host -foreground red "L'unita $DriveLetter non esiste, impossibile creare l'istanza"
		exit 2
	}
} 

function Exists-IP {
	param($Ip) 
	$Exists = $false
	(Get-WmiObject -Class Win32_NetworkAdapterConfiguration -Filter IPEnabled=TRUE -ComputerName .).IPAddress | ForEach-Object { if ($_ -eq "$Ip") { $Exists=$true; }}
	if (!$Exists) {
		Write-Host -foreground red "L'indirizzo ip specificato non � presente sulla macchina"
		exit 2
	}
} 

function Exists-AppDir {
	param($AppDir) 
	$FApplExists = Test-Path $AppDir
	if ($FApplExists) {
		Write-Host -foreground red "La directory di destinazione � gia presente"
		exit 2
	}
}

function Exists-IIS {
	$IISExists = Test-Path "IIS:\"
	if (!($IISExists)) {
		Write-Host -foreground red "Impossibile accedere a IIS:\, � stato caricato correttamente il modulo?"
		exit 2
	}
}

function Exists-Website {
	param($InstanceName) 
	$Exists = $false
  try {
    Get-ChildItem IIS:\Sites | ForEach-Object { if ($_.name -eq "$InstanceName") { $Exists=$true; }} | Out-Null
    if ($Exists) {
      Write-Host -foreground red "Il sito specificato � gia presente sulla macchina"
      exit 2
    }
  } catch {
    Write-Host -foreground red "Impossibile verificare i siti su IIS. E' stato caricato il Modulo Web?"
    exit 2
  }
} 

function Exists-ApplicationPool {
	param($InstanceName) 
	$Exists = $false
  try {
    Get-ChildItem IIS:\AppPools | ForEach-Object { if ($_.name -eq "$InstanceName") { $Exists=$true; }} | Out-Null
    if ($Exists) {
      Write-Host -foreground red "L'applicationPool specificato � gia presente sulla macchina"
      exit 2
    }
  } catch {
    Write-Host -foreground red "Impossibile verificare gli application pool di IIS. E' stato caricato il Modulo Web?"
    exit 2
  }
} 

if ($IpAddress -eq $null) {
  $IpAddresses = Get-WmiObject Win32_NetworkAdapterConfiguration | Where {$_.IPEnabled -eq "TRUE"}
  $validAnswer = $false
  do {
	  $counter = 1
	  Write-Host "Indirizzi IP disponibili"
	  foreach($Ip in $IpAddresses.IPAddress) {
		Write-Host "${counter}."${Ip}
		$counter++
	  }
	  $Answer = Read-Host "Quale indirizzo si desidera utilizzare?"
	  try {
	  $AnswerNumeric = [int]$Answer
	  if ($AnswerNumeric -ne 0 ) {
		  $AnswerNumeric--
		  if ($IpAddresses.IPAddress[$AnswerNumeric] -ne $null) {
			$validAnswer = $true
		  }
	  }
	  } catch {}
  } while (!$validAnswer)
  $IpAddress = $IpAddresses.IPAddress[$AnswerNumeric]
}

Valid-IP $IpAddress	
Exists-Drive (${SitesDir}).Substring(0,1)
Exists-IP $IpAddress

$Answer = Read-Host "Si desidera creare l'istanza ${InstanceName} con ip ${IpAddress}? (S/n)"
if ($Answer -eq "S") {
	$appPool32Bit = Read-Host "Si desidera impostare la compatibilita' a 32 bit? (S/n)"
  try {
    Import-Module WebAdministration
  } catch {
    Write-Host -foreground red "Impossibile caricare gli script per la gestione di IIS"
    exit 2
  }
	Exists-IIS
	Exists-Website $InstanceName
	Exists-ApplicationPool $InstanceName
	Exists-AppDir $AppDir
	$FApplExists = Test-Path $SitesDir
  try {
    if (!$FApplExists) {
      Write-Warning "Creo directory ${SitesDir} .."
      New-Item -type directory -path $SitesDir | Out-Null
    }
    Write-Warning "Creo directory ${AppDir} .."
    New-Item -type directory -path $AppDir | Out-Null
    Write-Warning "Creo directory ${StageAreaDir} .."
    New-Item -type directory -path $StageAreaDir | Out-Null
    Write-Warning "Creo directory ${LogsDir} .."
    New-Item -type directory -path $LogsDir | Out-Null
    Write-Warning "Creo directory ${HtdocsDir} .."
    New-Item -type directory -path $HtdocsDir | Out-Null
    Write-Warning "Creo directory ${HtdocsDir}\public .."
    New-Item -type directory -path $HtdocsDir\public | Out-Null
    Write-Warning "Creo file KeepAlive .."
    $ServerName = "Alive " + $env:computername
    [System.IO.File]::WriteAllText("${HtdocsDir}\public\keep-alive.html", $ServerName) | Out-Null
  } catch {
    Write-Host -foreground red "Impossibile creare la struttura delle directory"
    exit 2
  }

  try {
    Write-Warning "Setto permessi di ${AppDir} .."
    $acl = get-acl -path $AppDir
    $newAcl = "IIS_IUSRS","FullControl","ContainerInherit,ObjectInherit","None","Allow"
    $accessRule = new-object System.Security.AccessControl.FileSystemAccessRule $newAcl
    $acl.SetAccessRule($accessRule) | Out-Null
    $acl | Set-Acl $AppDir
  } catch {
    Write-Host -foreground red "Impossibile impostare i permessi della directory"
    exit 2
  }

  try {
  	Write-Warning "Creo AppPool ${InstanceName} .."
    New-Item IIS:\AppPools\${InstanceName} | Out-Null
  } catch {
    Write-Host -foreground red "Impossibile creare l'application pool"
    exit 2
  }

  try {
    $countWebSites = (Get-ChildItem IIS:\Sites | Measure-Object).count
    Write-Warning "Creo Sito ${InstanceName} .."
    if ($countWebSites -gt 0) {
      New-Website -Name ${InstanceName} -Port $Port -IPAddress $IpAddress -PhysicalPath ${HtdocsDir} -ApplicationPool ${InstanceName} | Out-Null
    } else {
      New-Website -id 1 -Name ${InstanceName} -Port $Port -IPAddress $IpAddress -PhysicalPath ${HtdocsDir} -ApplicationPool ${InstanceName} | Out-Null
    }
  } catch {
    Write-Host -foreground red "Impossibile creare il sito"
    exit 2
  }
  try {
    Write-Warning "Imposto autenticazione anonima con utente dell'application pool .."
    Set-WebConfigurationProperty /system.webServer/security/authentication/anonymousAuthentication -name userName -value "" -PSPath "IIS:\" -location ${InstanceName} | Out-Null
  } catch {
    Write-Host -foreground red "Impossibile cambiare il tipo di autenticazione"
    exit 2
  }
  try {
    Write-Warning "Imposto path dei file di log .."
    Set-ItemProperty IIS:\Sites\${InstanceName} -Name LogFile.directory -Value $LogsDir | Out-Null
  } catch {
    Write-Host -foreground red "Impossibile cambiare impostazioni Log"
    exit 2
  }

  if ($appPool32Bit -eq "S") {
	try {
	Write-Warning "Imposto AppPool compatibile a 32b .."
	set-itemProperty IIS:\apppools\${InstanceName} -name "enable32BitAppOnWin64" -Value "true" | Out-Null
	} catch {
	  Write-Host -foreground red "Impossibile modificare appPool a 32bit"
      exit 2
	}
  }

} else {
	Write-Host "Creazione istanza annullata"
}