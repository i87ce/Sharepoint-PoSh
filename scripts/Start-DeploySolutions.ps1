Write-Host "Working, Please wait...."
Add-PSSnapin microsoft.sharepoint.powershell -ErrorAction SilentlyContinue

###########################################################################
### Funzioni sui wsp singoli
###########################################################################
function UninstallSolution($wsp)
{
	$WSPFullFileName = $wsp.FullName
	$WSPFileName = $wsp.Name
	
	Write-Host "Uninstalling solution " -NoNewline
	Write-Host $WSPFileName
	$SPsolution = Get-SPSolution -Identity $WSPFileName -ErrorAction SilentlyContinue 
	if ($SPsolution -eq $null)
	{
		Write-Host ("Solution " + $WSPFileName + " not present") -ForegroundColor Yellow
	}
	elseif (!$SPsolution.Deployed)
	{
		Write-Host ("Solution " + $WSPFileName + " not installed") -ForegroundColor Yellow
	}
	elseif ($SPsolution.ContainsWebApplicationResource)
	{
		$DeployedWebApp = $solution.DeployedWebApplications | Select Url
		
		foreach ($DeployedWA in $DeployedWebApp)
		{
			Uninstall-SPSolution -Identity $WSPFileName -webapplication $DeployedWA -Confirm:$false
		}	
	}
	else
	{
		Uninstall-SPSolution -Identity $WSPFileName -Confirm:$false
	}
	
	WaitDeployProcess -wsp $wsp -deploying $False
}

function RemoveSolution($wsp, $Force) 
{
	$WSPFullFileName = $wsp.FullName
	$WSPFileName = $wsp.Name
	
    Write-Host "Removing solution " -NoNewline
	Write-Host $WSPFileName
	$SPsolution = Get-SPSolution -Identity $WSPFileName -ErrorAction SilentlyContinue
	if ($SPsolution -eq $null)
	{
		Write-Host ("Solution " + $WSPFileName + " not present") -ForegroundColor Yellow
	}
	else
	{
    	Remove-SPSolution -Identity $WSPFileName -ErrorAction Continue -Force:$Force -Confirm:$false 
	}
}

function AddSolution($wsp, $Retry)
{
	Write-Host -ForegroundColor White -BackgroundColor Blue "Action Add Solution" 

	$WSPFullFileName = $wsp.FullName
	$WSPFileName = $wsp.Name
	Write-Host -ForegroundColor White -BackgroundColor Blue "Working on $WSPFileName" 
	
	try
	{
		Write-Host -ForegroundColor Green "Checking Status of Solution"
		$output = Get-SPSolution -Identity $WSPFileName -ErrorAction Stop
	}
	Catch
	{
		$DoesSolutionExists = $_
	}
	If (($DoesSolutionExists -like "*Cannot find an SPSolution*") -and ($output.Name -notlike  "*$WSPFileName*"))
	{
		Try
		{
			Write-Host -ForegroundColor Green "Adding solution to farm"
			Add-SPSolution "$WSPFullFileName" -Confirm:$false -ErrorAction Stop | Out-Null
 
			Write-Host -ForegroundColor Green "Checking Status of Solution"
			$output = Get-SPSolution -Identity $WSPFileName -ErrorAction Stop
			If (($DoesSolutionExists -like "*Cannot find an SPSolution*") -and ($output.Name -notlike  "*$WSPFileName*"))
			{
				Write-Host -ForegroundColor Green "Solution Added"
			}
			else
			{
				Write-Host -ForegroundColor Red "Skipping $WSPFileName, Due to an error"
			}			
		}
		Catch
		{
			Write-Error $_
			Write-Host -ForegroundColor Red "Skipping $WSPFileName, Due to an error"
			Read-Host
		}
	}
	else
	{
		## Rimuovi la solution e rilancia
		UninstallSolution -wsp $wsp
		if ($Retry -eq $True)
		{
			AddSolution -wsp $wsp -Retry $False
		}
		else
		{
			Write-Host -ForegroundColor Red "Skipping $WSPFileName, Due to an error"
			Read-Host
		}
	}
	
}

function InstallSolution($wsp, $WebApp, $Force)
{
	$WSPFullFileName = $wsp.FullName
	$WSPFileName = $wsp.Name
	
	Write-Host "Installing solution " -NoNewline
	Write-Host $WSPFileName
	$SPsolution = Get-SPSolution -Identity $WSPFileName -ErrorAction SilentlyContinue 
	if ($SPsolution -eq $null)
	{
		Write-Host ("Solution " + $WSPFileName + " not present") -ForegroundColor Yellow
	}	
	try
	{
		Write-Host -ForegroundColor Green "Deploy solution to $WebApp, will skip if this solution is globally deployed"
		Install-SPSolution -Identity "$WSPFileName" -GACDeployment -WebApplication $WebApp -Force:$Force -Confirm:$false -ErrorAction Stop | Out-Null
	}
	Catch
	{
		$gobal = $_
		#Write-Host $gobal
	}
	if ($gobal -like "*FullTrustBinDeployment*")
	{
		Write-Host -ForegroundColor Yellow "Attention! This package needs to use Full Trust Bin Deployment that is MS Deprecated, do you want to continue? (y/N)"
		$OpFTBDChoose = Read-Host
		
		if (($OpFTBDChoose -eq "Y") -or ($OpFTBDChoose -eq "y"))
		{
			try
			{
				Write-Host -ForegroundColor Green "Deploy solution to $WebApp with FullTrustBinDeployment, will skip if this solution is globally deployed"
				Install-SPSolution -Identity "$WSPFileName" -FullTrustBinDeployment -GACDeployment -WebApplication $WebApp -Force:$Force -Confirm:$false -ErrorAction Stop | Out-Null
			}
			Catch
			{
				$gobal = $_
				Write-Host $gobal
			}
			If ($gobal -like "*This solution contains*")
			{
				Write-Host -ForegroundColor Green "Solution requires global deployment with FullTrustBinDeployment, Deploying now"
				Install-SPSolution -Identity "$WSPFileName" -FullTrustBinDeployment -GACDeployment -Force:$Force -Confirm:$false -ErrorAction Stop | Out-Null
			}
		}
		else
		{
			Write-Host -ForegroundColor Yellow "Package aborted"
		}
	}
	If ($gobal -like "*This solution contains*")
	{
		Write-Host -ForegroundColor Green "Solution requires global deployment, Deploying now"
		Install-SPSolution -Identity "$WSPFileName" -GACDeployment -Force:$Force -Confirm:$false -ErrorAction Stop | Out-Null
	}
	
	WaitDeployProcess -wsp $wsp -deploying $True
}

function UpdateSolution($wsp, $Force) 
{
	$WSPFullFileName = $wsp.FullName
	$WSPFileName = $wsp.Name
	
    Write-Host "Updating solution " -NoNewline
	Write-Host $WSPFileName
	$SPsolution = Get-SPSolution -Identity $WSPFileName -ErrorAction SilentlyContinue
	if ($SPsolution -eq $null)
	{
		Write-Host ("Solution " + $WSPFileName + " not present") -ForegroundColor Yellow
	}
	else
	{
		Update-SPSolution -Identity $WSPFileName -LiteralPath $WSPFullFileName -GACDeployment -ErrorAction Continue -Force:$Force
	}
}
###########################################################################

###########################################################################
### Funzione sui wsp multipli
###########################################################################
function UninstallSolution-Multi($WSPList)
{
	foreach($wsp in $WSPList) 
	{	
		UninstallSolution -wsp $wsp
	}
}

function RemoveSolution-Multi($WSPList)
{
	foreach($wsp in $WSPList) 
	{	
		RemoveSolution -wsp $wsp -Force $True
	}
}

function AddSolution-Multi($WSPList)
{
	foreach($wsp in $WSPList) 
	{	
		AddSolution -wsp $wsp -Retry $True
	}
}

function InstallSolution-Multi($WSPList, $WebApp)
{
	foreach($wsp in $WSPList) 
	{	
		InstallSolution -wsp $wsp -WebApp $WebApp -Force $True
	}
}

function InstallSolution-WebApp-Multi($WSPList, $WebAppList)
{
	foreach ($WAElement in $WebAppList)
	{
		Write-Host -ForegroundColor White -BackgroundColor Blue $WAElement
		InstallSolution-Multi -WSPList $WSPList -WebApp $WAElement
	}
}

function UpdateSolution-Multi($WSPList)
{
	foreach($wsp in $WSPList) 
	{	
		UpdateSolution -wsp $wsp -Force $True
	}
}

###########################################################################

###########################################################################
### Funzioni di processo
###########################################################################
function WaitDeployProcess-Multi($WSPList, $deploying)
{
	foreach($wsp in $WSPList) 
	{	
		WaitDeployProcess -wsp $wsp -deploying $deploying
	}
}

function WaitDeployProcess($wsp, $deploying)
{
	$errorOccurred = $false
	Write-Host "Waiting operation to end" -NoNewline
	do
	{
		$Installed = $true

		$WSPFullFileName = $wsp.FullName
		$WSPFileName = $wsp.Name
		
		$SPsolution = Get-SPSolution -Identity $WSPFileName -ErrorAction SilentlyContinue 

		if ($SPsolution -eq $null)
		{ 
			continue 
		}
		
		if ($SPsolution.JobExists) 
		{ 
			$Installed = $false 
		}

		if ($Installed) 
		{ 
			break 
		}
		
		sleep -Seconds 5
		Write-Host "." -NoNewline
	}
	while($Installed -eq $false)
	
	# CHECK DEPLOY RESULT
	$errorOccurred = $false

	$WSPFullFileName = $wsp.FullName
	$WSPFileName = $wsp.Name
	
	$SPsolution = Get-SPSolution -Identity $WSPFileName -ErrorAction SilentlyContinue 

	if ($SPsolution -eq $null)
	{ 
		continue 
	}
	
	if (($deploying -and !$SPsolution.Deployed) -or (!$deploying -and $SPsolution.Deployed))
	{ 
		$errorOccurred = $true 
	}
	
	Write-Host ""
	if ($errorOccurred)
	{
		Write-Host "An error occurred during the solution deployment." -ForegroundColor Red
		Read-Host
	}
	else
	{
		Write-Host "Finished" -ForegroundColor Black -BackgroundColor Green
		Read-Host
	}
}

function SubMenuWebApplication
{
	do
	{
		clear
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
		Write-Host -ForegroundColor White -BackgroundColor Blue "Selected Web Apps: "
		foreach ($WAElement in $WAList)
		{
			Write-Host -ForegroundColor White -BackgroundColor Blue $WAElement
		}		
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
		Write-Host "Select one of the available options:"
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
		$wapps = Get-SPWebApplication -IncludeCentralAdministration
		
		$i=1
		
		foreach ($wa in $wapps)
		{
			Write-Host "$i)"$wa.Url
			$i += 1
		}
		Write-Host "0) Back"
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
		$OpChoiceSubMenu = Read-Host
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
		
		if ($OpChoiceSubMenu -ne "0")
		{
			$WebApp = $wapps[$OpChoiceSubMenu-1].Url

			Write-Host "Add (1), Remove (2), Cancel Selection (3)"
			$OpConfirm = Read-Host
			if ($OpConfirm -eq "1")
			{
				$WAList.Add($WebApp) 
				Write-Host -ForegroundColor Green "Added!"
			}	
			elseif ($OpConfirm -eq "2")
			{
				$WAList.Remove($WebApp) 
				Write-Host -ForegroundColor Green "Removed!"
			}
		}
	}
	while ($OpChoiceSubMenu -ne "0")
}
###########################################################################

###########################################################################
### Funzioni Grafiche
###########################################################################
function FormatString($StringValue, $MaxLength, $Left, $FillChar)
{
	if ($StringValue -eq $null)
	{ $StringValue = "" }
	
	if ($StringValue.Length > $MaxLength)
	{ $StringValue = $StringValue.Substring(0, $MaxLength)}
	
	if ($Left)
	{return $StringValue.PadLeft($MaxLength, $FillChar)}
	else
	{return $StringValue.PadRight($MaxLength, $FillChar)}
}

function PrintChoiceMenu($Title, $Choices)
{
	Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '#')
	Write-Host $Title
	Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '#')
	for ($i = 0; $i -lt $Choices.Length; $i++)
	{
		Write-Host $Choices[$i]
	}
	Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '#')
	$OpChoice = Read-Host 
	Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '#')
	return $OpChoice
}

###########################################################################

function Main($RootPath)
{
	$WAList = New-Object System.Collections.ArrayList
	$ChoosedPath = $False
	do
	{
		clear
		Write-Host -ForegroundColor White -BackgroundColor Blue "Selected Web Application $($WAList.count):"
		foreach ($WAElement in $WAList)
		{
			Write-Host -ForegroundColor White -BackgroundColor Blue $WAElement
		}
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '#')
		Write-Host -ForegroundColor White -BackgroundColor Blue "Selected WSP $($WSPList.count):"
		Foreach ($wsp in $WSPList)
		{
			$WSPFileName = $wsp.Name
			Write-Host -ForegroundColor White -BackgroundColor Blue "$WSPFileName"
		}
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '#')
		$MainTitle = "Select one of the available options:"
		$MainChoices = @(
			"a) Choose WSP Directory",
			"b) Choose Web Application",
			"##############################",
			"2) Uninstall WSP",
			"3) Remove WSP",
			"4) Add WSP",
			"5) Deploy WSP",
			"6) Update WSP",
			"0) Exit")
		
		$MainOpChoice = PrintChoiceMenu -Title $MainTitle -Choices $MainChoices
		
		if ($MainOpChoice -eq "a")
		{
			Write-Host "Use Repository? (Y/n)"
			$OpScriptPath = Read-Host
			
			if (($OpScriptPath -eq "n") -or ($OpScriptPath -eq "N"))
			{
				Write-Host "Please, Enter wsp path"
				$InstallDIR = Read-Host
				$ChoosedPath = $True
			}
			else
			{
				[System.Reflection.Assembly]::LoadWithPartialName("System.windows.forms") | Out-Null

				$OpenFileDialog = New-Object System.Windows.Forms.OpenFileDialog
				$OpenFileDialog.initialDirectory = "\\epts-repository.eni.com\pydio\"
				$OpenFileDialog.filter = "Wsp Solution (*.wsp)| *.wsp"
				$OpenFileDialog.ShowHelp = $true
				$OpenFileDialog.ShowDialog() | Out-Null			
				$InstallDIR = Split-Path -parent $OpenFileDialog.filename
				$ChoosedPath = $True
			}
			
			$Dir = get-childitem $InstallDIR -Recurse
			$WSPList = $Dir | where {$_.Name -like "*.wsp*"}
			Write-Host -ForegroundColor White -BackgroundColor Blue "Found this solution: "
			Write-Host ""
			
			Foreach ($wsp in $WSPList)
			{
				$WSPFileName = $wsp.Name
				Write-Host -ForegroundColor White -BackgroundColor Blue "$WSPFileName"
			}
			Write-Host ""
		}
		elseif ($MainOpChoice -eq "b")
		{
			SubMenuWebApplication
		}
		elseif ($MainOpChoice -eq "2")
		{
			if ($ChoosedPath -eq $True)
			{
				UninstallSolution-Multi -WSPList $WSPList
			}
			else
			{
				Write-Host -ForegroundColor White -BackgroundColor Red "Please, select first a wsp path"
				Read-Host
			}
		}
		elseif ($MainOpChoice -eq "3")
		{
			if ($ChoosedPath -eq $True)
			{
				RemoveSolution-Multi -WSPList $WSPList
			}
			else
			{
				Write-Host -ForegroundColor White -BackgroundColor Red "Please, select first a wsp path"
				Read-Host
			}
		}
		elseif ($MainOpChoice -eq "4")
		{
			if ($ChoosedPath -eq $True)
			{
				AddSolution-Multi -WSPList $WSPList
			}
			else
			{
				Write-Host -ForegroundColor White -BackgroundColor Red "Please, select first a wsp path"
				Read-Host
			}
		}		
		elseif ($MainOpChoice -eq "5")
		{
			if ($ChoosedPath -eq $True)
			{	
				InstallSolution-WebApp-Multi -WSPList $WSPList -WebAppList $WAList
			}
			else
			{
				Write-Host -ForegroundColor White -BackgroundColor Red "Please, select first a wsp path"
				Read-Host
			}
		}		
		elseif ($MainOpChoice -eq "6")
		{
			if ($ChoosedPath -eq $True)
			{
				UpdateSolution-Multi -WSPList $WSPList
			}
			else
			{
				Write-Host -ForegroundColor White -BackgroundColor Red "Please, select first a wsp path"
				Read-Host
			}
		}		
	}
	while ($MainOpChoice -ne "0")
	
}

main -RootPath ($myinvocation.mycommand.path | Split-Path)