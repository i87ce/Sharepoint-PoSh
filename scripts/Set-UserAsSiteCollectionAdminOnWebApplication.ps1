param
(
	[Parameter(Mandatory=$true, HelpMessage='username in format DOMAIN\username')]
	[string]$Username = "",
	[Parameter(Mandatory=$true, HelpMessage='url for web application e.g. http://collab')]
	[string]$WebApplicationUrl = ""
	
)
Add-PSSnapin Microsoft.SharePoint.PowerShell -erroraction SilentlyContinue
Write-Host "Setting up user $Username as site collection admin on all sitecollections in Web Application $WebApplicationUrl" -ForegroundColor White;
$webApplication = Get-SPWebApplication $WebApplicationUrl;

if($webApplication -ne $null)
{

foreach($siteCollection in $webApplication.Sites){
    Write-Host "Setting up user $Username as site collection admin for $siteCollection" -ForegroundColor White;
    $userToBeMadeSiteCollectionAdmin = $siteCollection.RootWeb.EnsureUser($Username);
    if($userToBeMadeSiteCollectionAdmin.IsSiteAdmin -ne $true)
    {
        $userToBeMadeSiteCollectionAdmin.IsSiteAdmin = $true;
        $userToBeMadeSiteCollectionAdmin.Update();
        Write-Host "User is now site collection admin for $siteCollection" -ForegroundColor Green;
    }
    else
    {
        Write-Host "User is already site collection admin for $siteCollection" -ForegroundColor DarkYellow;
    }

    Write-Host "Current Site Collection Admins for site: " $siteCollection.Url " " $siteCollection.RootWeb.SiteAdministrators;
}
}
else
{
	Write-Host "Could not find Web Application $WebApplicationUrl" -ForegroundColor Red;
}
