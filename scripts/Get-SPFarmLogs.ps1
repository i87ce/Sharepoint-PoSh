﻿if ((Get-PSSnapin "Microsoft.SharePoint.PowerShell" -ErrorAction SilentlyContinue) -eq $null) {
    Add-PSSnapin "Microsoft.SharePoint.PowerShell"
  }
function FormatString($StringValue, $MaxLength, $Left, $FillChar)
{
	if ($StringValue -eq $null)
	{ $StringValue = "" }
	
	if ($StringValue.Length > $MaxLength)
	{ $StringValue = $StringValue.Substring(0, $MaxLength)}
	
	if ($Left)
	{return $StringValue.PadLeft($MaxLength, $FillChar)}
	else
	{return $StringValue.PadRight($MaxLength, $FillChar)}
}

Function CollectLive($FilePath)
{
	Set-SPLogLevel -TraceSeverity VerboseEx -EventSeverity Verbose
	$TimeStart = Get-Date -UFormat "%d/%m/%Y %H:%M"
	$TimeStartstr = $TimeStart.ToString()
	Write-Host $TimeStartstr
	
	do
	{
		 $Confirm = Read-Host "Stop Collect? (Y)"
	} 
	Until (($Confirm -eq "Y") -or ($Confirm -eq "y"))
	
	$TimeStop = Get-Date -UFormat "%d/%m/%Y %H:%M"
	$TimeStopstr = $TimeStop.ToString()
	Write-Host $TimeStopstr
	#Clear-SpLogs
	&".\Set-SPLogsEvent.ps1"
	
	#Merge-SPLogFile -Path $FilePath -Overwrite -StartTime $TimeStart –EndTime $TimeStop
	CollectHistory -FilePath $FilePath -TimeStart $TimeStart -TimeStop $TimeStop
}

Function CollectHistory ($TimeStart, $TimeStop, $FilePath)
{
	Merge-SPLogFile -Path $FilePath -Overwrite -StartTime $TimeStart.ToString() –EndTime $TimeStop.ToString()
}

Function CollectHistoryByID($CorrelationID, $FilePath)
{
	Write-Host "..." $FilePath
	Merge-SPLogFile -Path $FilePath -Overwrite -Correlation $CorrelationID
}

Function FindInLocalLogs($CorrelationID)
{
	Get-ChildItem –Path G:\Logs\Farm\* -recurse | Select-String -pattern $CorrelationID | group path | select name
}

function PrintChoiceMenu($Title, $Choices)
{
	Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
	Write-Host $Title
	Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
	for ($i = 0; $i -lt $Choices.Length; $i++)
	{
		Write-Host $Choices[$i]
	}
	Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
	$OpChoice = Read-Host 
	Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
	return $OpChoice
}

function Main($RootPath)
{
	$FilePath = $RootPath + "FarmMergedLog.log"
	Write-Host "Collect Log to " $FilePath
	do
	{
		$MainTitle = "Select one of the available options:"
		$MainChoices = @(
			"1 ) Live Collect Log",
			"2 ) Collect Past Log (Interval Time)",
			"3 ) Collect Past Log (Correlation ID)",
			"4 ) Find in local Log (Correlation ID)",
			"0 ) Exit")
		
		$OpChoice = PrintChoiceMenu -Title $MainTitle -Choices $MainChoices
	
		
		if ($OpChoice -eq "0")
		{
			break
		}
		elseif ($OpChoice -eq "1")
		{
			CollectLive -FilePath $FilePath
			continue
		}
		elseif ($OpChoice -eq "2")
		{
			$TimeStart = Read-Host "Time Start? (dd/mm/yyyy hh:mm)"
			$TimeStop = Read-Host "Time Stop? (dd/mm/yyyy hh:mm)"
			CollectHistory -TimeStart $TimeStart -TimeStop $TimeStop -FilePath $FilePath
			continue
		}
		elseif ($OpChoice -eq "3")
		{
			$CorrelationID = Read-Host "Enter correlation ID"
			CollectHistoryByID -CorrelationID $CorrelationID -FilePath $FilePath
			continue
		}
		elseif ($OpChoice -eq "3")
		{
			$CorrelationID = Read-Host "Enter correlation ID"
			FindInLocalLogs -CorrelationID $CorrelationID
			continue
		}
		else
		{
			Write-Host "Unknown option"
			continue
		}
	}
	while (1)
}


main -RootPath ($myinvocation.mycommand.path | Split-Path)

write-host "Reloading..."
powershell 