if ((Get-PSSnapin "Microsoft.SharePoint.PowerShell" -ErrorAction SilentlyContinue) -eq $null) {
    Add-PSSnapin "Microsoft.SharePoint.PowerShell"
  }
$tmpPath = "G:\temp.bak"

$Confirm = Read-Host "Start Backup? (Y/N)"
if ($Confirm -eq "Y")
{
	$OldSiteCollection = Read-Host "Enter Site Collection URL"
	Write-Host ("Start backup of: " + $OldSiteCollection) -foreground "Yellow";
	Backup-SPSite $OldSiteCollection -Path $tmpPath -UseSqlSnapshot
	Write-Host ("Backup Terminato") -foreground "Green";
}

$Confirm = Read-Host "Start Restore? (Y/N)"
if ($Confirm -eq "Y")
{
	$NewSiteCollection = Read-Host "Enter Site Collection URL"
	$Confirm = Read-Host "Choose Content DB? (Y/N)"
	if ($Confirm -eq "Y")
	{
		$ContentDBName = Read-Host "Enter Content DB Name"
			$Confirm = Read-Host "Create It? (Y/N)"
			if ($Confirm -eq "Y")
			{
				$SQLConn = Read-Host "Enter sql server path"
				$WebApp =  Read-Host "Enter WebApplication url"
				$ContentDBName = Read-Host "Enter Content DB Name"
				Write-Host ("Start creation of: " + $ContentDBName) -foreground "Yellow";
				New-SPContentDatabase $ContentDBName -DatabaseServer $SQLConn -WebApplication $WebApp
				Write-Host ("Creation completed") -foreground "Green";
			}
		Write-Host ("Start restore on: " + $ContentDBName + " of: " + $NewSiteCollection) -foreground "Yellow";
		Restore-SPSite $NewSiteCollection -Path $tmpPath -ContentDatabase $ContentDBName -Force -Confirm:$false
		Write-Host ("Restore completed") -foreground "Green";
	}
	else
	{
		Write-Host ("Start restore of: " + $NewSiteCollection) -foreground "Yellow";
		Restore-SPSite $NewSiteCollection -Path $tmpPath -Force
		Write-Host ("Restore completed") -foreground "Green";
	}
}
$Confirm = Read-Host "Delete backup file? (Y/N)"
if ($Confirm -eq "Y")
{
	Remove-Item $tmpPath -Confirm:$false
}
Read-Host "Bye!"
Remove-PsSnapin Microsoft.SharePoint.PowerShell
