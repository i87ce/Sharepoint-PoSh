﻿<#	
	.NOTES
	===========================================================================
	 Created with: 	SAPIEN Technologies, Inc., PowerShell Studio 2015 v4.2.81
	 Created on:   	21/05/2015 17:00
	 Created by:   	Alessio Vigilante alessio.vigilante@techroad.it
	 Organization: 	TechRoad
	 Filename:     	SetPeoplePickerOneWay.ps1
	===========================================================================
	.DESCRIPTION
		https://support.microsoft.com/en-us/kb/2874332
		https://technet.microsoft.com/en-us/library/gg602075.aspx
		https://technet.microsoft.com/en-us/library/cc263460(office.12).aspx

[System.Security.SecureString]$secureStringValue = Read-Host "Enter the account password: " -AsSecureString
#>

#region Function
#region Utility
function FormatString($StringValue, $MaxLength, $Left, $FillChar)
{
	if ($StringValue -eq $null)
	{ $StringValue = "" }
	
	if ($StringValue.Length -gt $MaxLength)
	{ $StringValue = $StringValue.Substring(0, $MaxLength) }
	
	if ($Left)
	{ return $StringValue.PadLeft($MaxLength, $FillChar) }
	else
	{ return $StringValue.PadRight($MaxLength, $FillChar) }
}
function GetSharepointVersion()
{
	$SPVersion = ""
	$SPVersion = (Get-PSSnapin microsoft.sharepoint.powershell).Version.Major
	return "$SPVersion.0"
}
#endregion
#region Properties
function SetServerProperties()
{
	stsadm -o setapppassword -password Password11
}

function SetFarmProperties()
{
	# Enable the global setting in the farm. You have to do this part only one time.
	$farm = get-spfarm
	$farm.Properties
	$farm.Properties["disable-netbios-dc-resolve"] = $true
	$farm.Properties
	$farm.Update()
}

function SetWebAppDomain($DomainName, $ShortDomainName, $LoginName, [System.Security.SecureString]$secureStringValue)
{
	$newdomain = new-object Microsoft.SharePoint.Administration.SPPeoplePickerSearchActiveDirectoryDomain
	$newdomain.DomainName = $DomainName # specify the fqdn
	$newdomain.ShortDomainName = $ShortDomainName # Specify the netbios name.
	$newdomain.loginname = $LoginName # Specify an account that has access to the remote domain.
	$newdomain.setpassword($secureStringValue)
	$newdomain.isForest = $True
	$wa.PeoplePickerSettings.SearchActiveDirectoryDomains.Add($newdomain)
}

function SetWebApplication($WebApplicationUrl, $Enviroment)
{
	if (($Enviroment -eq "1") -or ($Enviroment -eq "2") -or ($Enviroment -eq "3"))
	{
		$BckFile = $WebApplicationUrl
		$BckFile = $BckFile -replace "http://", ""
		$BckFile = $BckFile -replace "/", ""
		$BckFile = $BckFile -replace ":", ""
		
		# Handle one webapplication.
		$wa = Get-SPWebApplication $WebApplicationUrl
		$wa.PeoplePickerSettings.SearchActiveDirectoryDomains | out-file "Logs\$BckFile.txt"
		$wa.PeoplePickerSettings.SearchActiveDirectoryDomains.Clear()
		
		#produzione
		if ($Enviroment -eq "1")
		{
			$securePasswordENINET = (ConvertTo-SecureString "Vp3FF0Sc" -AsPlainText -Force)
			$securePasswordENI = (ConvertTo-SecureString "TCzQ7G2jn3" -AsPlainText -Force)
			SetWebAppDomain -DomainName "eni.pri" -ShortDomainName "eni" -LoginName "eni\ADMSPPeoplePicker" -secureStringValue $securePasswordENI
			SetWebAppDomain -DomainName "eni.intranet" -ShortDomainName "eninet" -LoginName "eninet\ADMINSPPEOPLEPICKER" -secureStringValue $securePasswordENINET
		}
		#system-test
		elseif ($Enviroment -eq "2")
		{
			$securePasswordENINET = (ConvertTo-SecureString "ZSFZt66b" -AsPlainText -Force)
			$securePasswordENI = (ConvertTo-SecureString "YPjI6ttxTS" -AsPlainText -Force)
			SetWebAppDomain -DomainName "eni.pri" -ShortDomainName "eni" -LoginName "eni\ADMSPPeoplePickert" -secureStringValue $securePasswordENI
			SetWebAppDomain -DomainName "eni.intranet" -ShortDomainName "eninet" -LoginName "eninet\ADMINSPPEOPLEPICKERT" -secureStringValue $securePasswordENINET
		}
		#developer
		elseif ($Enviroment -eq "3")
		{
			$securePasswordENINET = (ConvertTo-SecureString "12OBLOHD" -AsPlainText -Force)
			$securePasswordENI = (ConvertTo-SecureString "yZAbjY2T7b" -AsPlainText -Force)
			SetWebAppDomain -DomainName "eni.pri" -ShortDomainName "eni" -LoginName "eni\ADMSPPeoplePickerd" -secureStringValue $securePasswordENI
			SetWebAppDomain -DomainName "eni.intranet" -ShortDomainName "eninet" -LoginName "eninet\ADMINSPPEOPLEPICKERD" -secureStringValue $securePasswordENINET
		}
		$wa.update()
	}
	else
	{
		Write-Host -ForegroundColor 'Red' "Wrong Enviroment!"
	}
}
#endregion
#region SubMenuWebApplication
function SubMenuWebApplication($Enviroment)
{
	do
	{
		$Exit = "0"
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
		Write-Host "Select one of the available options:"
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
		$wapps = Get-SPWebApplication -IncludeCentralAdministration
#		$wapps = Get-SPAlternateURL | select PublicUrl
#		
#		$wapps | where-Object { $_.tostring() -notlike "*:*" } | ForEach-Object { if ($_.tostring() -like "https://") } |  Select-Object -Unique
		
		$i=1
		
		foreach ($wa in $wapps)
		{
			Write-Host "$i)"$wa.Url
			$i += 1
		}
		Write-Host "a) All Web Application"
		Write-Host "b) Insert Web App Identity"
		Write-Host "0) Exit"
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
		$OpChoiceSubMenu = Read-Host
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
		
		
		if ($OpChoiceSubMenu -eq "0")
		{
			$Exit = "1"
			continue
		}
		elseif ($OpChoiceSubMenu -eq "a")
		{
			Write-Host "Setting All Web Apps, are you sure? (Y/N)"
			$OpConfirm = Read-Host
			if (($OpConfirm -eq "Y") -and ($OpConfirm -eq "y"))
			{
				foreach ($wa in $wapps)
				{
					Write-Host $wa.Url
					SetWebApplication -WebApplicationUrl $wa.Url -Enviroment $Enviroment
				}
				Write-Host -ForegroundColor Green "Done!"
			}
		}
		elseif ($OpChoiceSubMenu -eq "b")
		{
			Write-Host "Please, insert web app identity:"
			$WebAppUrl = Read-Host
			Write-Host "Setting $WebAppUrl, are you sure? (Y/N)"
			$OpConfirm = Read-Host
			if (($OpConfirm -eq "Y") -and ($OpConfirm -eq "y"))
			{

				Write-Host $WebAppUrl
				SetWebApplication -WebApplicationUrl $WebAppUrl -Enviroment $Enviroment

				Write-Host -ForegroundColor Green "Done!"
			}
		}
		else
		{
			Write-Host "Setting"$wapps[$OpChoiceSubMenu-1].Url" are you sure? (Y/N)"
			$OpConfirm = Read-Host
			if (($OpConfirm -ne "Y") -and ($OpConfirm -ne "y"))
			{
				continue
			}
			SetWebApplication -WebApplicationUrl $wapps[$OpChoiceSubMenu-1].Url -Enviroment $Enviroment
			Write-Host -ForegroundColor Green "Done!"
		}
		
	}
	while ($Exit -eq "0")
}
#endregion
#region SubMenuEnviroment
function SubMenuEnviroment()
{
	do
	{
		$Exit = "0"
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
		Write-Host "Select one of the available options:"
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
		Write-Host "1) Produzione"
		Write-Host "2) System-Test"
		Write-Host "3) Developer"
		Write-Host "0) Exit"
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
		$OpChoiceSubMenu = Read-Host
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
		
		
		if ($OpChoiceSubMenu -eq "0")
		{
			$Exit = "1"
			continue
		}
		SubMenuWebApplication -Enviroment $OpChoiceSubMenu
		Write-Host -ForegroundColor Green "Done!"
	}
	while ($Exit -eq "0")
}
#endregion
#region SetRegistryACL
function SetRegistryACL($Version)
{
	$acl = Get-Acl "HKLM:\SOFTWARE\Microsoft\Shared Tools\Web Server Extensions\$Version\Secure"
	$rule = New-Object System.Security.AccessControl.RegistryAccessRule ("$env:computername\WSS_WPG", "ReadKey", "ContainerInherit, ObjectInherit", "None", "Allow")
	$acl.SetAccessRule($rule)
	$rule = New-Object System.Security.AccessControl.RegistryAccessRule ("$env:computername\WSS_Admin_WPG", "FullControl", "ContainerInherit, ObjectInherit", "None", "Allow")
	$acl.SetAccessRule($rule)
	$rule = New-Object System.Security.AccessControl.RegistryAccessRule ("$env:computername\WSS_RESTRICTED_WPG_V4", "FullControl", "ContainerInherit, ObjectInherit", "None", "Allow")
	$acl.SetAccessRule($rule)
	$acl | Set-Acl -Path "HKLM:\SOFTWARE\Microsoft\Shared Tools\Web Server Extensions\$Version\Secure"
	
}
#endregion
#endregion

function Main($ScriptPath)
{
	
	if ((Get-PSSnapin | Where { $_.Name -eq "Microsoft.SharePoint.PowerShell" }) -eq $null)
	{
		Add-PSSnapin Microsoft.SharePoint.PowerShell;
	}
	
	do
	{
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
		$Version = GetSharepointVersion
		Write-Host "Sharepoint Version: $Version"
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
		Write-Host "Select one of the available options:"
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
		Write-Host "1) Set Local Server Encrypt Key"
		Write-Host "2) Set Sharepoint Farm Properties"
		Write-Host "3) Set Web Application Properties"
		Write-Host "0) Exit"
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
		$OpChoice = Read-Host
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
		
		if ($OpChoice -eq "0")
		{
			Exit 0
		}
		elseif ($OpChoice -eq "1")
		{
			Write-Host "Set Local Server Encrypt Key, are you sure? (Y/N)"
			$OpConfirm = Read-Host
			if (($OpConfirm -ne "Y") -and ($OpConfirm -ne "y"))
			{
				continue
			}
			Write-Host -ForegroundColor Yellow "Starting Operation..."
			SetServerProperties
			SetRegistryACL -Version $Version
			Write-Host -ForegroundColor Green "Operation End";
		}
		elseif ($OpChoice -eq "2")
		{
			Write-Host "Set Sharepoint Farm Properties, are you sure? (Y/N)"
			$OpConfirm = Read-Host
			if (($OpConfirm -ne "Y") -and ($OpConfirm -ne "y"))
			{
				continue
			}
			Write-Host -ForegroundColor Yellow "Starting Operation..."
			SetFarmProperties
			Write-Host -ForegroundColor Green "Operation End";
		}
		elseif ($OpChoice -eq "3")
		{
			Write-Host -ForegroundColor Yellow "Starting Operation..."
			SubMenuEnviroment
			Write-Host -ForegroundColor Green "Operation End";
		}
	}
	while (1)
}

$ScriptPath = $myinvocation.mycommand.path | Split-Path
main -ScriptPath $ScriptPath
		
