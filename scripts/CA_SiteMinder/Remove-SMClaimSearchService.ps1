# Copyright (c) 2011 CA.  All rights reserved.
#
# This software and all information contained therein is confidential
# and proprietary and shall not be duplicated, used, disclosed or 
# disseminated in any way except as authorized by the applicable
# license agreement, without the express written permission of CA.
# All authorized reproductions must be marked with this language.  
#
# EXCEPT AS SET FORTH IN THE APPLICABLE LICENSE AGREEMENT, TO THE
# EXTENT PERMITTED BY APPLICABLE LAW, CA PROVIDES THIS SOFTWARE
# WITHOUT WARRANTY OF ANY KIND, INCLUDING WITHOUT LIMITATION, ANY
# IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR
# PURPOSE.  IN NO EVENT WILL CA BE LIABLE TO THE END USER OR ANY
# THIRD PARTY FOR ANY LOSS OR DAMAGE, DIRECT OR INDIRECT, FROM THE
# USE OF THIS SOFTWARE, INCLUDING WITHOUT LIMITATION, LOST PROFITS,
# BUSINESS INTERRUPTION, GOODWILL, OR LOST DATA, EVEN IF CA IS
# EXPRESSLY ADVISED OF SUCH LOSS OR DAMAGE.


param([Parameter(Position=0, Mandatory=$true)][String]$WebApplication)

$webApp = Get-SPWebApplication $WebApplication 
if($webApp -eq $null)
{
	"Error: Cannot find web application for given url: $WebApplication"
	return
}

#Find the WebConfigModification added by us.
$owner = "CASiteMinderSharePoint2010Agent_ClaimsSearchServiceEndpoint"
$found = $false
do
{
	$removeWebConfigModification = $null
	foreach($webConfigModification in $webApp.WebConfigModifications)
	{
		if($webConfigModification.Owner -eq $owner)
		{
			$removeWebConfigModification = $webConfigModification
			$found = $true
		}
	}

	if($removeWebConfigModification -eq $null)
	{
		if($found -eq $false)
		{
			"INFO: Did not find any web.config modification in web application : $WebApplication made by CA SiteMinder SharePoint 2010 Agent."
			return
		}
		
	}
	
	$webApp.WebConfigModifications.Remove($removeWebConfigModification)
	$webApp.update()

}while($removeWebConfigModification -ne $null)

if($webApp.IsAdministrationWebApplication -eq $true)
{
	$service = [Microsoft.SharePoint.Administration.SPWebService]::AdministrationService

}
else
{
	$service = [Microsoft.SharePoint.Administration.SPWebService]::ContentService
}


"INFO: Applying web.config modifications"
$service.ApplyWebConfigModifications()  

