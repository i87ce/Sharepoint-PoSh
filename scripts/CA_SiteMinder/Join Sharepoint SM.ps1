$rootcert = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2("V:\scripts\CA_SiteMinder\zlab-ca.crt")
New-SPTrustedRootAuthority -Name "ZLAB CA" -Certificate $rootcert 

$cert = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2("V:\scripts\CA_SiteMinder\idp.siteminder-pem.cer")
New-SPTrustedRootAuthority -Name "Siteminder IDP" -Certificate $cert

$map1 = New-SPClaimTypeMapping -IncomingClaimType "http://schemas.xmlsoap.org/claims/useridentifier" -IncomingClaimTypeDisplayName "Siteminder User" -SameAsIncoming


$realm = "urn:ENFRSP14D_TrustedIDP"
$signinurl = "https://sd-tsclaim.eni.com:443/affwebservices/public/wsfeddispatcher"
$ap = New-SPTrustedIdentityTokenIssuer -Name "SiteMinder" -Description "SiteMinder Metadirectory Federation" -realm $realm -ImportTrustCertificate $cert -ClaimsMappings $map1 -SignInUrl $signinurl -IdentifierClaim $map1.InputClaimType -UseWReply

$tip = Get-SPTrustedIdentityTokenIssuer "SiteMinder"
$tip.ClaimProviderName = "CASiteMinderClaimProvider";
$tip.Update()

$sts = Get-SPSecurityTokenServiceConfig
$sts.LogonTokenCacheExpirationWindow = (New-TimeSpan �hours 1)
$sts.UseSessionCookies = $true
$sts.Update()



.\Set-SMClaimProviderConfiguration.ps1 -DisableLoopBackSearch
.\Set-SMClaimProviderConfiguration.ps1 -UserNameFormat DisplaynameAppended
.\Set-SMClaimProviderConfiguration.ps1 -GroupNameFormat DisplaynameAppended

.\Add-SMClaimSearchService.ps1 -WebApplication https://welldiary.eni.com -claimSearchService https://tsclaim.eni.com:2443/ClaimsWS/services/WSSharePointClaimsServiceImpl
.\Add-SMClaimSearchService.ps1 -WebApplication http://t4srv1015r:10000/ -claimSearchService https://tsclaim.eni.com:2443/ClaimsWS/services/WSSharePointClaimsServiceImpl

.\Add-SMClaimSearchService.ps1 -WebApplication https://sd-welldiary.eni.com -claimSearchService http://sd-tsclaim.eni.com:2080/ClaimsWS/services/WSSharePointClaimsServiceImpl
.\Add-SMClaimSearchService.ps1 -WebApplication http://t4srv1017d:10000/ -claimSearchService http://sd-tsclaim.eni.com:2080/ClaimsWS/services/WSSharePointClaimsServiceImpl


iisreset



$cert = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2("V:\scripts\CA_SiteMinder\prod\SM_TokenSign_Prod.cer")
$tip = Get-SPTrustedIdentityTokenIssuer
$tip | Set-SPTrustedIdentityTokenIssuer -ImportTrustCertificate $cert
$tip.Update()
Remove-SPTrustedRootAuthority -Identity "ZLAB CA" -Confirm:$False
Remove-SPTrustedRootAuthority -Identity "Siteminder IDP" -Confirm:$False
New-SPTrustedRootAuthority -Name "Siteminder IDP" -Certificate $cert
