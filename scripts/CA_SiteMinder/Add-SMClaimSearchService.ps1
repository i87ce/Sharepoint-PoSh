# Copyright (c) 2011 CA.  All rights reserved.
#
# This software and all information contained therein is confidential
# and proprietary and shall not be duplicated, used, disclosed or 
# disseminated in any way except as authorized by the applicable
# license agreement, without the express written permission of CA.
# All authorized reproductions must be marked with this language.  
#
# EXCEPT AS SET FORTH IN THE APPLICABLE LICENSE AGREEMENT, TO THE
# EXTENT PERMITTED BY APPLICABLE LAW, CA PROVIDES THIS SOFTWARE
# WITHOUT WARRANTY OF ANY KIND, INCLUDING WITHOUT LIMITATION, ANY
# IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR
# PURPOSE.  IN NO EVENT WILL CA BE LIABLE TO THE END USER OR ANY
# THIRD PARTY FOR ANY LOSS OR DAMAGE, DIRECT OR INDIRECT, FROM THE
# USE OF THIS SOFTWARE, INCLUDING WITHOUT LIMITATION, LOST PROFITS,
# BUSINESS INTERRUPTION, GOODWILL, OR LOST DATA, EVEN IF CA IS
# EXPRESSLY ADVISED OF SUCH LOSS OR DAMAGE.

param([Parameter(Position=0, Mandatory=$true)][String]$WebApplication, 
		[Parameter(Position=1, Mandatory=$true)][String]$ClaimSearchService,
		[Parameter(ParameterSetName="ClientAuth")][Switch]$EnableSSLClientAuthentication,
		[Parameter(ParameterSetName="ClientAuth")][ValidateNotNullOrEmpty()][String]$ClientCertificateName)

$owner =  "CASiteMinderSharePoint2010Agent_ClaimsSearchServiceEndpoint"
$behaviorName = "CASiteMinderClaimProviderCertificate"
$clientCredentialType = "None"
$securityMode = "None"

$searchService = New-Object System.Uri($ClaimSearchService)
$isHttps = $false
if($searchService.Scheme -eq "https")
{
	$isHttps = $true
}

#Throw error if cient authentication is enabled but client search service does not have a https URL
if(($isHttps -eq $false) -and ($EnableSSLClientAuthentication -eq $true))
{
	Throw "ERROR: Client authentication is enabled but Claims serach service URL scheme is not https"
	return
}

if($isHttps -eq $true)
{
	$securityMode = "Transport"
}

if($EnableSSLClientAuthentication)
{
	if($ClientCertificateName -eq "")
	{
		Throw "Missing argument ClientCertificateName. SSL client authentication is enabled but client certificate name specified."
	}
	$clientCredentialType = "Certificate"
}

#compose endpoint configuration
$bindings = "<bindings>"
$bindings += "</bindings>"

$basicHttpBinding = "<basicHttpBinding>"
$basicHttpBinding += "</basicHttpBinding>"

$binding = "<binding name=`"WSSharePointClaimsServiceImplSoapBinding`" closeTimeout=`"00:01:00`""
$binding += " openTimeout=`"00:01:00`" receiveTimeout=`"00:10:00`" sendTimeout=`"00:01:00`""
$binding += " allowCookies=`"false`" bypassProxyOnLocal=`"false`" hostNameComparisonMode=`"StrongWildcard`""
$binding += " maxBufferSize=`"65536`" maxBufferPoolSize=`"524288`" maxReceivedMessageSize=`"65536`""
$binding += " messageEncoding=`"Text`" textEncoding=`"utf-8`" transferMode=`"Buffered`""
$binding += " useDefaultWebProxy=`"true`">"
$binding += " <readerQuotas maxDepth=`"32`" maxStringContentLength=`"8192`" maxArrayLength=`"16384`""
$binding += " maxBytesPerRead=`"4096`" maxNameTableCharCount=`"16384`" />"
$binding += " <security mode=`"$securityMode`">"
$binding += " <transport clientCredentialType=`"$clientCredentialType`" proxyCredentialType=`"None`""
$binding += " realm=`"`" />"
$binding += " <message clientCredentialType=`"UserName`" algorithmSuite=`"Default`" />"
$binding += " </security>"
$binding += " </binding>"

$client = "<client>"
$client += "</client>"

$endpoint = "<endpoint address=`"$ClaimSearchService`" binding=`"basicHttpBinding`" bindingConfiguration=`"WSSharePointClaimsServiceImplSoapBinding`""
$endpoint += " contract=`"SiteMinderClaimSearchService.WSSharePointClaimsServiceImpl`" "
if($EnableSSLClientAuthentication)
{
	$endpoint += " behaviorConfiguration=`"$behaviorName`" "
}
$endpoint += " name=`"WSSharePointClaimsServiceImpl`" />"

$webApp = Get-SPWebApplication $WebApplication 
if($webApp -eq $null)
{
	Throw "Error: Cannot find web application for given url: $WebApplication"
	return
}

$modification = New-Object -TypeName "Microsoft.SharePoint.Administration.SPWebConfigModification" 
$modification.Path = "configuration/system.serviceModel"
$modification.Name = "bindings"
$modification.Value = $bindings  
$modification.Owner = $owner
$modification.Sequence = 0  
$modification.Type = [Microsoft.SharePoint.Administration.SPWebConfigModification+SPWebConfigModificationType]::EnsureSection  

$clientTagModification = New-Object -TypeName "Microsoft.SharePoint.Administration.SPWebConfigModification" 
$clientTagModification.Path = "configuration/system.serviceModel"
$clientTagModification.Name = "client"
$clientTagModification.Value = $client  
$clientTagModification.Owner = $owner
$clientTagModification.Sequence = 0  
$clientTagModification.Type = [Microsoft.SharePoint.Administration.SPWebConfigModification+SPWebConfigModificationType]::EnsureSection  

$basicHttpBindingModification = New-Object -TypeName "Microsoft.SharePoint.Administration.SPWebConfigModification" 
$basicHttpBindingModification.Path = "configuration/system.serviceModel/bindings"
$basicHttpBindingModification.Name = "basicHttpBinding"
$basicHttpBindingModification.Value = $basicHttpBinding  
$basicHttpBindingModification.Owner = $owner 
$basicHttpBindingModification.Sequence = 1  
$basicHttpBindingModification.Type = [Microsoft.SharePoint.Administration.SPWebConfigModification+SPWebConfigModificationType]::EnsureSection  

$bindingModification = New-Object -TypeName "Microsoft.SharePoint.Administration.SPWebConfigModification" 
$bindingModification.Path = "configuration/system.serviceModel/bindings/basicHttpBinding"
$bindingModification.Name = "binding[@name='WSSharePointClaimsServiceImplSoapBinding']"
$bindingModification.Value = $binding
$bindingModification.Owner = $owner
$bindingModification.Sequence = 2  
$bindingModification.Type = [Microsoft.SharePoint.Administration.SPWebConfigModification+SPWebConfigModificationType]::EnsureChildNode 

$endPointModification = New-Object -TypeName "Microsoft.SharePoint.Administration.SPWebConfigModification" 
$endPointModification.Path = "configuration/system.serviceModel/client"
$endPointModification.Name = "endpoint[@name='WSSharePointClaimsServiceImpl']"
$endPointModification.Value = $endpoint  
$endPointModification.Owner = $owner
$endPointModification.Sequence = 3
$endPointModification.Type = [Microsoft.SharePoint.Administration.SPWebConfigModification+SPWebConfigModificationType]::EnsureChildNode  

$webApp.WebConfigModifications.Add($modification)  
$webApp.WebConfigModifications.Add($clientTagModification)
$webApp.WebConfigModifications.Add($basicHttpBindingModification)  
$webApp.WebConfigModifications.Add($bindingModification)  
$webApp.WebConfigModifications.Add($endPointModification)  

if($EnableSSLClientAuthentication)
{
	
	"INFO: Adding web config modifications for SSL mutual trust"

	$behaviorsTag = "<behaviors>"
	$behaviorsTag += "</behaviors>"

	$endpointBehaviorsTag = "<endpointBehaviors>"
	$endpointBehaviorsTag += "</endpointBehaviors>"

	$behaviorNameTag = "<behavior name=`"$behaviorName`">"
	$behaviorNameTag +="<clientCredentials>"
	$behaviorNameTag +="<clientCertificate storeName=`"My`" storeLocation=`"LocalMachine`" findValue=`"$ClientCertificateName`" x509FindType=`"FindBySubjectName`"/>" 
	$behaviorNameTag +="</clientCredentials>"
	$behaviorNameTag +="</behavior>"

	$behaviorsTagModification = New-Object -TypeName "Microsoft.SharePoint.Administration.SPWebConfigModification" 
	$behaviorsTagModification.Path = "configuration/system.serviceModel"
	$behaviorsTagModification.Name = "behaviors"
	$behaviorsTagModification.Value = $behaviorsTag  
	$behaviorsTagModification.Owner = $owner
	$behaviorsTagModification.Sequence = 0  
	$behaviorsTagModification.Type = [Microsoft.SharePoint.Administration.SPWebConfigModification+SPWebConfigModificationType]::EnsureSection 

	$endpointBehaviorsTagModification = New-Object -TypeName "Microsoft.SharePoint.Administration.SPWebConfigModification" 
	$endpointBehaviorsTagModification.Path = "configuration/system.serviceModel/behaviors"
	$endpointBehaviorsTagModification.Name = "endpointBehaviors"
	$endpointBehaviorsTagModification.Value = $endpointBehaviorsTag  
	$endpointBehaviorsTagModification.Owner = $owner
	$endpointBehaviorsTagModification.Sequence = 1  
	$endpointBehaviorsTagModification.Type = [Microsoft.SharePoint.Administration.SPWebConfigModification+SPWebConfigModificationType]::EnsureSection 

	$behaviorNameTagModification = New-Object -TypeName "Microsoft.SharePoint.Administration.SPWebConfigModification" 
	$behaviorNameTagModification.Path = "configuration/system.serviceModel/behaviors/endpointBehaviors"
	$behaviorNameTagModification.Name = "behavior[@name='$behaviorName']"
	$behaviorNameTagModification.Value = $behaviorNameTag
	$behaviorNameTagModification.Owner = $owner
	$behaviorNameTagModification.Sequence = 2  
	$behaviorNameTagModification.Type = [Microsoft.SharePoint.Administration.SPWebConfigModification+SPWebConfigModificationType]::EnsureChildNode 

	$webApp.WebConfigModifications.Add($behaviorsTagModification	)
	$webApp.WebConfigModifications.Add($endpointBehaviorsTagModification)
	$webApp.WebConfigModifications.Add($behaviorNameTagModification)

}

$webApp.Update()  

if($webApp.IsAdministrationWebApplication -eq $true)
{
	$service = [Microsoft.SharePoint.Administration.SPWebService]::AdministrationService

}
else
{
	$service = [Microsoft.SharePoint.Administration.SPWebService]::ContentService
}

$service.ApplyWebConfigModifications()


"Applied web.config modifications to web application: $WebApplication"

