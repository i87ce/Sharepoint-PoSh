# Copyright (c) 2011 CA.  All rights reserved.
#
# This software and all information contained therein is confidential
# and proprietary and shall not be duplicated, used, disclosed or 
# disseminated in any way except as authorized by the applicable
# license agreement, without the express written permission of CA.
# All authorized reproductions must be marked with this language.  
#
# EXCEPT AS SET FORTH IN THE APPLICABLE LICENSE AGREEMENT, TO THE
# EXTENT PERMITTED BY APPLICABLE LAW, CA PROVIDES THIS SOFTWARE
# WITHOUT WARRANTY OF ANY KIND, INCLUDING WITHOUT LIMITATION, ANY
# IMPLIED WARRANTIES OF MERCHANTABILITY OR FITNESS FOR A PARTICULAR
# PURPOSE.  IN NO EVENT WILL CA BE LIABLE TO THE END USER OR ANY
# THIRD PARTY FOR ANY LOSS OR DAMAGE, DIRECT OR INDIRECT, FROM THE
# USE OF THIS SOFTWARE, INCLUDING WITHOUT LIMITATION, LOST PROFITS,
# BUSINESS INTERRUPTION, GOODWILL, OR LOST DATA, EVEN IF CA IS
# EXPRESSLY ADVISED OF SUCH LOSS OR DAMAGE.

param([Parameter(ParameterSetName="enableLoopBack")][Switch]$EnableLoopBackSearch,
[Parameter(ParameterSetName="disableLoopBack")][Switch]$DisableLoopBackSearch,
[Parameter(ParameterSetName="help")][Switch]$Help,
[Parameter(ParameterSetName="userNameFormat", Mandatory=$true)]
[ValidateSet("ValueOnly", "DisplaynameOnly", "DisplaynameAppended")]
[String]$UserNameFormat,
[Parameter(ParameterSetName="groupNameFormat", Mandatory=$true)]
[ValidateSet("ValueOnly", "DisplaynameOnly", "DisplaynameAppended")]
[String]$GroupNameFormat
)

[Void][System.Reflection.Assembly]::LoadWithPartialName("CASiteMinderClaimProvider")

$configuration = [CA.SiteMinder.SharePoint2010.SMClaimProviderConfiguration]::Local

if($configuration -eq $null)
{
	"ERROR: Failed to read configuration."
	return
}

switch ($PsCmdlet.ParameterSetName) 
{ 
	"enableLoopBack"{
			$configuration.IsLoopBackSearchEnabled = $true
			$configuration.Update()
			"INFO: Configuration updated successfully"
			break
	} 
	"disableLoopBack"{	
			$configuration.IsLoopBackSearchEnabled = $false
			$configuration.Update()
			"INFO: Configuration updated successfully"
			break
	} 
	"userNameFormat"{  
			switch($UserNameFormat)
			{
				"ValueOnly" {
							$configuration.UserNameFormat = [CA.SiteMinder.SharePoint2010.UserDisplaynameFormat]::ValueOnly
							$configuration.Update()
							"INFO: Configuration updated successfully"
							break
							}
				"DisplaynameOnly" {
							$configuration.UserNameFormat = [CA.SiteMinder.SharePoint2010.UserDisplaynameFormat]::DisplaynameOnly
							$configuration.Update()
							"INFO: Configuration updated successfully"
							break
							}
				"DisplaynameAppended" {
							$configuration.UserNameFormat = [CA.SiteMinder.SharePoint2010.UserDisplaynameFormat]::DisplaynameAppended
							$configuration.Update()
							"INFO: Configuration updated successfully"
							break
							}
			}
			break
	}
	"groupNameFormat"{
			switch($GroupNameFormat)
			{
				"ValueOnly" {
							$configuration.GroupNameFormat = [CA.SiteMinder.SharePoint2010.GroupDisplaynameFormat]::ValueOnly
							$configuration.Update()
							"INFO: Configuration updated successfully"
							break
							}
				"DisplaynameOnly" {
							$configuration.GroupNameFormat = [CA.SiteMinder.SharePoint2010.GroupDisplaynameFormat]::DisplaynameOnly
							$configuration.Update()
							"INFO: Configuration updated successfully"
							break
							}
				"DisplaynameAppended" {
							$configuration.GroupNameFormat = [CA.SiteMinder.SharePoint2010.GroupDisplaynameFormat]::DisplaynameAppended
							$configuration.Update()
							"INFO: Configuration updated successfully"
							break}
			}
			break
	}
	"help" {
			".\Set-SMClaimProviderConfiguration.ps1 [-EnableLoopBackSearch|-DisableLoopBackSearch|-UserNameFormat <format type>|-GroupNameFormat <format type>]"
			"	-EnableLoopBackSearch	:	Enables loop back search in claim provider"
			"	-DisableLoopBackSearch	:	Disables loop back search in claim provider"
			"	-UserNameFormat		:	Sets the display name format for users in claim provider. It accepts three values only"
			"	                                ValueOnly			- displays user name (identifier claim value only) in people picker"
			"	                                DisplaynameOnly		- displays user display name only (as configured in SiteMinder user directory) in people picker"
			"	                                DisplaynameAppended	- displays user name (identifier claim value) appended with user display name"
			"	-GroupNameFormat	:	Sets the display name format for SiteMinder groups in claim provider. It accepts three values only"
			"	                                ValueOnly			- displays group name (claim value only) in people picker"
			"	                                DisplaynameOnly		- displays group display name only (as configured in SiteMinder user directory) in people picker"
			"	                                DisplaynameAppended	- displays group name (claim value) appended with user display name"
			break
	}
	"default" { "ERROR: No valid parameters found"; break}
} 