if ((Get-PSSnapin "Microsoft.SharePoint.PowerShell" -ErrorAction SilentlyContinue) -eq $null)
{
	Add-PSSnapin "Microsoft.SharePoint.PowerShell"
}
Write-Host -ForegroundColor 'Magenta' "Please, insert web app identity"
$wa = Read-Host

if ($wa -ne $null)
{
	get-spwebapplication -identity $wa | foreach { $SPContentDB = $_.ContentDatabases }
	$SPContentDB | ForEach { Dismount-SPContentDatabase $_ -confirm:$false }
}