function FormatString($StringValue, $MaxLength, $Left, $FillChar)
{
	if ($StringValue -eq $null)
	{ $StringValue = "" }
	
	if ($StringValue.Length -gt $MaxLength)
	{ $StringValue = $StringValue.Substring(0, $MaxLength) }
	
	if ($Left)
	{ return $StringValue.PadLeft($MaxLength, $FillChar) }
	else
	{ return $StringValue.PadRight($MaxLength, $FillChar) }
}

function InstallPatch($installPath, $installName)
{
	Write-Host "Installing..." -ForegroundColor Green
	
	$path = Join-Path $installPath $installName
	
	$AppInstall = (Start-Process -Wait -PassThru $path)
	
	switch ($AppInstall.ExitCode)
	{
		0 {
			Write-Host "Successfully installed" -ForegroundColor Green
		}
		default
		{
			Write-Host "An error has occured. Code: " $AppInstall.ExitCode -ForegroundColor Red
		}
	}
	
	return $AppInstall.ExitCode
}

function InstallFindPatch($InitialPath)
{
	$ListaPatch = Get-ChildItem -Path $InitialPath -Filter "*.exe" -Recurse | Sort-Object Directory
	
	if ($ListaPatch.Count -gt 0)
	{
		foreach ($Patch in $ListaPatch)
		{
			InstallPatch -installPath $Patch.DirectoryName -installName $Patch.Name
		}
	}
}

function Main($ScriptPath)
{
	if ((Get-PSSnapin "Microsoft.SharePoint.PowerShell" -ErrorAction SilentlyContinue) -eq $null)
	{
		Add-PSSnapin "Microsoft.SharePoint.PowerShell"
	}
	
	Write-Host "Wich Enviroment? Developement (1), Test (2), Production (3)" -ForegroundColor Magenta
	$OpConfirm = Read-Host
	
	if ($OpConfirm -eq "1")
	{
		$SMEnv = "Developement"
		$SMUrl = "sd-tsclaim.eni.com"
		$SMRealm = "urn:ENFRSP14D_TrustedIDP"
		$CAName = "ZLAB CA"
		$CAPath = "$ScriptPath\CA_SiteMinder\dev\zlab-ca.crt"
		$ChainName = "Siteminder IDP"
		$ChainPath = "$ScriptPath\CA_SiteMinder\dev\idp.siteminder-pem.cer"
	}
	elseif ($OpConfirm -eq "2")
	{
		$SMEnv = "Test"
		$SMUrl = "st-tsclaim.eni.com"
		$SMRealm = "urn:ENFRSPT_TrustedIDP"
		$CAName = "ZLAB CA"
		$CAPath = "$ScriptPath\CA_SiteMinder\test\zlab-ca.crt"
		$ChainName = "Siteminder IDP"
		$ChainPath = "$ScriptPath\CA_SiteMinder\test\idp.siteminder-pem.cer"
	}
	elseif ($OpConfirm -eq "3")
	{
		$SMEnv = "Production"
		$SMUrl = "tsclaim.eni.com"
		$SMRealm = "urn:ENFRSP_TrustedIDP"
		$CAName = "ZLAB CA"
		$CAPath = "$ScriptPath\CA_SiteMinder\prod\zlab-ca.crt"
		$ChainName = "Siteminder IDP"
		$ChainPath = "$ScriptPath\CA_SiteMinder\prod\idp.siteminder-pem.cer"
	}
	
	Write-Host "Do you connect to SM Agent using HTTP (1) or HTTPS (2)" -ForegroundColor Magenta
	$OpConfirm = Read-Host
	
	if ($OpConfirm -eq "1")
	{
		$SMProtocol = "http"
		$SMPort = "80"
		$SMClaimPort = "2080"
	}
	elseif ($OpConfirm -eq "2")
	{
		$SMProtocol = "https"
		$SMPort = "443"
		$SMClaimPort = "2443"
	}
	
	$signinurl = $SMProtocol + "://" + $SMUrl + ":" + $SMPort + "/affwebservices/public/wsfeddispatcher"
	$ClaimUrl = $SMProtocol + "://" + $SMUrl + ":" + $SMClaimPort + "/ClaimsWS/services/WSSharePointClaimsServiceImpl"
	$CentralAdmin = Get-SPWebApplication -includecentraladministration | where { $_.IsAdministrationWebApplication } | Select-Object -ExpandProperty Url
	
	do
	{
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '#')
		Write-Host "Enviroment: $SMEnv" -ForegroundColor Green
		Write-Host "Protocol: $SMProtocol" -ForegroundColor Green
		Write-Host "Select one of the available options:"
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '#')
		Write-Host "1) Install CA Agent for Sharepoint"
		Write-Host "2) Configure CA Agent for Sharepoint"
		Write-Host "3) Add a new webapplication on siteminder"
		Write-Host "4) Remove CA Agent for Sharepoint configuration"
		Write-Host "5) Upgrade Siteminder IDP Certificate"
		Write-Host "0) Exit"
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '#')
		$OpChoiceSubOne = Read-Host
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '#')
		
		if ($OpChoiceSubOne -eq "1")
		{
			Write-Host "Do you want to install the CA Client? (Y/N)"
			$OpConfirm = Read-Host
			if (($OpConfirm -eq "Y") -and ($OpConfirm -eq "y"))
			{
				Write-Host -ForegroundColor Yellow "Installa Client CA SiteMinder"
				InstallFindPatch -InitialPath "$ScriptPath\CA_SiteMinder"
			}
		}
		elseif ($OpChoiceSubOne -eq "2")
		{
			Write-Host "Do you want to configure the CA Client? (Y/N)"
			$OpConfirm = Read-Host
			if (($OpConfirm -eq "Y") -and ($OpConfirm -eq "y"))
			{
				Write-Host -ForegroundColor Yellow "Configura Sharepoint con il Client CA SiteMinder"
				Write-Host -ForegroundColor Yellow "Importo i certificati di trust"
				$rootcert = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2($CAPath)
				New-SPTrustedRootAuthority -Name $CAName -Certificate $rootcert
				$cert = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2($ChainPath)
				New-SPTrustedRootAuthority -Name $ChainName -Certificate $cert
				
				$map1 = New-SPClaimTypeMapping -IncomingClaimType "http://schemas.xmlsoap.org/claims/useridentifier" -IncomingClaimTypeDisplayName "Siteminder User" -SameAsIncoming
				$realm = $SMRealm
				
				#$signinurl = "https://" + $SMUrl + ":443/affwebservices/public/wsfeddispatcher"
				
				$ap = New-SPTrustedIdentityTokenIssuer -Name "SiteMinder" -Description "SiteMinder Metadirectory Federation" -realm $realm -ImportTrustCertificate $cert -ClaimsMappings $map1 -SignInUrl $signinurl -IdentifierClaim $map1.InputClaimType -UseWReply
				
				$tip = Get-SPTrustedIdentityTokenIssuer "SiteMinder"
				$tip.ClaimProviderName = "CASiteMinderClaimProvider"
				$tip.Update()
				
				$sts = Get-SPSecurityTokenServiceConfig
				#$sts.LogonTokenCacheExpirationWindow = (New-TimeSpan �minutes 10)
				$sts.LogonTokenCacheExpirationWindow = (New-TimeSpan �hours 1)
				$sts.UseSessionCookies = $true
				$sts.Update()
				
				.\CA_SiteMinder\Set-SMClaimProviderConfiguration.ps1 -DisableLoopBackSearch
				.\CA_SiteMinder\Set-SMClaimProviderConfiguration.ps1 -UserNameFormat DisplaynameAppended
				.\CA_SiteMinder\Set-SMClaimProviderConfiguration.ps1 -GroupNameFormat DisplaynameAppended
				
				.\CA_SiteMinder\Add-SMClaimSearchService.ps1 -WebApplication $CentralAdmin -claimSearchService $ClaimUrl
				Write-Host -ForegroundColor Green "Configurazione Sharepoint con il Client CA SiteMinder Terminata"
				Write-Host -ForegroundColor Red "Fase Ended! - Please Provide an IISReset on all server of the farm"
			}
		}
		elseif ($OpChoiceSubOne -eq "3")
		{
			Write-Host "Please provide webapplication as full url (http://)"
			$OpUrl = Read-Host
			Write-Host -ForegroundColor Yellow "Adding WebApplication Config CA SiteMinder"
			.\CA_SiteMinder\Add-SMClaimSearchService.ps1 -WebApplication $OpUrl -claimSearchService $ClaimUrl
			$ap = New-SPAuthenticationProvider -TrustedIdentityTokenIssuer (Get-SPTrustedIdentityTokenIssuer "SiteMinder")
			Set-SPWebApplication $OpUrl -AuthenticationProvider $ap -Zone "Default"
			Write-Host -ForegroundColor Green "Added WebApplication Config CA SiteMinder"
		}
		elseif ($OpChoiceSubOne -eq "4")
		{
			Write-Host "Do you want to remove the configuration? (Y/N)"
			$OpConfirm = Read-Host
			if (($OpConfirm -eq "Y") -and ($OpConfirm -eq "y"))
			{
				Write-Host -ForegroundColor Yellow "Remove Config CA SiteMinder"
				$ap = New-SPAuthenticationProvider -UseWindowsIntegratedAuthentication
				$contentWebAppServices = (Get-SPFarm).services | ? { $_.typename -eq "Microsoft SharePoint Foundation Web Application" }
				
				foreach ($webApp in $contentWebAppServices.WebApplications)
				{
					foreach ($zone in $webapp.IisSettings.Keys)
					{
						$authProviders = Get-SPAuthenticationProvider -WebApplication $webApp -Zone $zone
						if (($authProviders | ? { $_.DisplayName -eq "SiteMinder" }) -ne $null)
						{
							Write-Host -ForegroundColor Yellow "Removing " + $authProviders.DisplayName
							Set-SPWebApplication $webApp -AuthenticationProvider $ap -Zone $zone
							Write-Host -ForegroundColor Green "Removing " + $authProviders.DisplayName
						}
					}
				}
				Remove-SPTrustedRootAuthority -Identity $CAName
				Remove-SPTrustedRootAuthority -Identity $ChainName
				Get-SPTrustedIdentityTokenIssuer "SiteMinder" | Remove-SPTrustedIdentityTokenIssuer
				Write-Host -ForegroundColor Green "Removed Config CA SiteMinder"
			}
		}
		elseif ($OpChoiceSubOne -eq "5")
		{
			### TBD
			Write-Host "Do you want to update the certificate? (Y/N)"
			$OpConfirm = Read-Host
			if (($OpConfirm -eq "Y") -and ($OpConfirm -eq "y"))
			{
				Write-Host -ForegroundColor Yellow "Update certificate"
				Remove-SPTrustedRootAuthority CASigningRootCert
				Remove-SPTrustedRootAuthority CASigningCert
				
				$rootcert = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2("full_path_to_updated_certificate_authority_certificate.cer")
				$cert = New-Object System.Security.Cryptography.X509Certificates.X509Certificate2("full_path_to_signing_certificate.cer")
				$tip = Get-SPTrustedIdentityTokenIssuer name_of_trusted_identity_provider
				$tip.SigningCertificate = $cert
				$tip.Update()
				New-SPTrustedRootAuthority -Name "CASigningRootCert" -Certificate $rootcert
				New-SPTrustedRootAuthority -Name "CASigningCert" -Certificate $cert
			}
		}
	}
	while ($OpChoiceSubOne -ne "0")
	Remove-PsSnapin Microsoft.SharePoint.PowerShell
}

$ScriptPath = $myinvocation.mycommand.path | Split-Path
Main -ScriptPath $ScriptPath