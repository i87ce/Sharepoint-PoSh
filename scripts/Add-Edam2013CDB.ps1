﻿if ((Get-PSSnapin "Microsoft.SharePoint.PowerShell" -ErrorAction SilentlyContinue) -eq $null) {
    Add-PSSnapin "Microsoft.SharePoint.PowerShell"
  }

#Get available quota templates in farm
$Templates = [Microsoft.SharePoint.Administration.SPWebService]::ContentService.QuotaTemplates

Function GetNextTemplateName ([string]$ActualTemplateName)
{
	#Possible Value Template
	#34642 - DLW_Minimal
	#17988 - DLW_Standard
	#34209 - DLW_Large
	#46915 - DLW_XL
	#62727 - DLW_XXL
	$value = "" | Select-Object -Property Name,DBPrefix,Limit
	if ([string]::IsNullOrEmpty($ActualTemplateName))
	{
		$value.Name = "DLW_Minimal"
		$value.DBPrefix = "WSS_Content_Edam_S*"
		$value.Limit = 40
	}
	if ($ActualTemplateName -eq "DLW_Minimal")
	{
		$value.Name = "DLW_Standard"
		$value.DBPrefix = "WSS_Content_Edam_M*"
		$value.Limit = 10
	}
	if ($ActualTemplateName -eq "DLW_Standard")
	{
		$value.Name = "DLW_Large"
		$value.DBPrefix = "WSS_Content_Edam_L*"
		$value.Limit = 4
	}
	if ($ActualTemplateName -eq "DLW_Large")
	{
		$value.Name = "DLW_XL"
		$value.DBPrefix = "WSS_Content_Edam_XL*"
		$value.Limit = 2
	}
	if ($ActualTemplateName -eq "DLW_XL")
	{
		$value.Name = "DLW_XXL"
		$value.DBPrefix = "WSS_Content_Edam_XXL*"
		$value.Limit = 1
	}
	return $value
}

Function Send-Email {
    [CmdletBinding()]
    Param(
        [Parameter(Position=0,Mandatory=$true)]
        [String]$Subject="",
        [Parameter(Position=1,Mandatory=$true)]
        [Object]$To=@(),
        [Parameter(Position=2,Mandatory=$false)]
        [Object]$Cc="",
        [Parameter(Position=3,Mandatory=$false)]
        [String]$Body="",
        [Parameter(Position=4,Mandatory=$false)]
        [String]$from="UpgradeEdamQuota@eni.com",
        [Parameter(Position=5,Mandatory=$false)]
        [String]$domain="relay.services.eni.intranet"
    )
    Process {
        $mail = New-Object System.Net.Mail.MailMessage
        for($i=0; $i -lt $To.Length; $i++) {
            $mail.To.Add($To[$i]);
        }
		for($i=0; $i -lt $Cc.Length; $i++) {
            $mail.Cc.Add($Cc[$i]);
        }
        $mail.From = New-Object System.Net.Mail.MailAddress($from)
        $mail.Subject = $Subject
		$mail.IsBodyHtml = 1
        $mail.Body = $Body
        $smtp = New-Object System.Net.Mail.SmtpClient($domain)
        $smtp.Send($mail)
        $mail.Dispose()
    }
}

Function CalcDBIndex([int]$Index) {
 [string]$value = "";
 [int]$id = $Index + 1;
 
	if ($id -lt 10)
	{
		$value = "00" + $id.ToString();
	}
	if (($id -gt 9) -and ($id -lt 100))
	{
		$value = "0" + $id.ToString();
	}
	if (($id -gt 99) -and ($id -lt 1000))
 	{
		$value = $id.ToString();
	}
	
 return $value
}

Function Main
{
	$Index = ""
	do
	{
		$Tmp = GetNextTemplateName($Index)
	
	    if (!([string]::IsNullOrEmpty($Tmp.Name)))
        {
    		$NextContentDB = Get-SPContentDatabase -WebApplication http://edam2013.eni.com | Where-Object {$_.name -like $Tmp.DBPrefix -and $_.CurrentSiteCount -lt $_.MaximumSiteCount} | select -last 1
    		if ([string]::IsNullOrEmpty($NextContentDB.Name))
    		{
    			#No New Content DB
    			$LastDB = Get-SPContentDatabase -WebApplication http://edam2013.eni.com | Where-Object {$_.name -like $Tmp.DBPrefix} | sort-object -property Name | select -last 1
    			$tmpDBNamePath = $Tmp.DBPrefix.Substring(0,$Tmp.DBPrefix.Length-1)
    			$tmpDBNameIndex = ($LastDB.Name -split '_')[4]
    			$tmpDBNameIndex = CalcDBIndex([int]$tmpDBNameIndex)
    			$NewContentDBName =  $tmpDBNamePath + "_" + $tmpDBNameIndex
    			Write-Host "Create ContentDB Name: "$NewContentDBName  -foreground "Yellow"
    				
    			New-SPContentDatabase $NewContentDBName -DatabaseServer "EPTS-SHP10F09-EDAM-DB1-PR.services.eni.intranet\CDB06" -WebApplication http://edam2013.eni.com
    		
    			Write-Host "ContentDB Creation Completed" -foreground "Green"
			
				Set-SPContentDatabase $NewContentDBName -MaxSiteCount $Tmp.Limit -WarningSiteCount 0

				Write-Host "Set Content DB Limit Completed" -foreground "Green"


    			$Oggetto = "Nuovo Content Database EDAM2013"
    			$Corpo = " è stato creato un nuovo content database da backuppare nella farm ENFRSP09 `r`n"
    			$Corpo += "il nuovo Database si chiama: " + $NewContentDBName + " `r`n"
    					
    			$To = @("sws_webapp_epts@hp.com", "goc_bur_epts@hp.com", "gian-paolo.garatti@hpe.com")
    			Send-Email -Subject $Oggetto -Body $Corpo -To $To
    				
    			$NextContentDB = Get-SPContentDatabase -WebApplication http://edam2013.eni.com | Where-Object {$_.name -like $NextTemplate.DBPrefix -and $_.CurrentSiteCount -lt $_.MaximumSiteCount} | select -last 1
    		}
	    }
	
		$Index = $Tmp.Name
	}
	while (!([string]::IsNullOrEmpty($Index)))

	Write-Host "Bye!" -foreground "Green"
}

Main;

Remove-PsSnapin Microsoft.SharePoint.PowerShell


