#################################################
# Add a new Farm Administrator
#################################################
Add-PSSnapin *SharePoint* -erroraction SilentlyContinue

function AddWebAppUser($WebApplicationUrl, $userOrGroup)
{
	$web = Get-SPWebApplication $WebApplicationUrl
	write-host "Add user $userOrGroup" -fore cyan 
	$policy = $web.Policies.Add($userOrGroup, "")
	$policyRole = $web.PolicyRoles.GetSpecialRole([Microsoft.SharePoint.Administration.SPPolicyRoleType]::FullControl)
	$policy.PolicyRoleBindings.Add($policyRole)	
	$web.Update()
}

Write-Host
$newUser = "eni\GG.Profiles.ASA_PCK_COLLABORATION-MOET-CC-II.ENI"
$newUserDomain = ($newUser -split "\\")[0]
$newUserAccount = ($newUser -split "\\")[1]
$listaserver = get-spserver | ? { $_.Role -eq "Application" }


Write-Host



	Write-Host "Add user to CA Admin"
	$adminwebapp = Get-SPwebapplication -includecentraladministration | where {$_.IsAdministrationWebApplication}  
	$adminsite = Get-SPweb($adminwebapp.Url)  
	$admingroup = $adminsite.AssociatedOwnerGroup  
	write-host "Adding user $newUser to the SP farm admin group" -fore cyan  
	$adminsite.SiteGroups[$admingroup].AddUser($newUser,"","","")  
	write-host "User $newUser added to successfully to the SP farm admin group" -fore green 
	
	Write-Host "Add user to Sharepoint Powershell Admin"
	$contentDB = Get-SPContentDatabase -WebApplication $adminwebapp
	Add-SPShellAdmin -Database $contentDB -Username $newUser

	Write-Host "Added to CA Admin! Continue as Local Admin"
	
	$group = "Administrators"
	foreach ($server in $listaserver)
	{
		$pc = $server.Address
		Write-Host "Aggiungo User: "$newUserDomain"\"$newUserAccount" al server: "$pc
		$objUser = [ADSI]("WinNT://$newUserDomain/$newUserAccount")
		$objGroup = [ADSI]("WinNT://$pc/$group")
		$objGroup.PSBase.Invoke("Add",$objUser.PSBase.Path)
	}
	
	Write-Host "Added to Local Admin! Continue as Web App User Policy Full Control"
	$wapps = Get-SPWebApplication
	foreach ($wa in $wapps)
	{
		Write-Host $wa.Url
		AddWebAppUser -WebApplicationUrl $wa.Url -userOrGroup $newUser
	}
	
	Write-Host "Completed Succesfully!"
	Read-Host
