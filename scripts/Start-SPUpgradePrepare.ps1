function FormatString($StringValue, $MaxLength, $Left, $FillChar)
{
	if ($StringValue -eq $null)
	{ $StringValue = "" }
	
	if ($StringValue.Length -gt $MaxLength)
	{ $StringValue = $StringValue.Substring(0, $MaxLength) }
	
	if ($Left)
	{ return $StringValue.PadLeft($MaxLength, $FillChar) }
	else
	{ return $StringValue.PadRight($MaxLength, $FillChar) }
}

Function MountDBByCSV($PathCSV)
{

	Import-Csv $PathCSV -Delimiter ";" |`
		ForEach-Object {
			$WebApp = $_.WebApp
			$DBIstance = $_.DBIstance
			$DBName = $_.DBName
			$DBLimit = $_.DBLimit
			$DBNewID = $_.DBNewID
			

			if ($DBNewID -eq "1")
			{
				write-host "Aggiungo il db $DBName $(date) con un nuovo ID..."
				Mount-SPContentDatabase $DBName -DatabaseServer $DBIstance -WebApplication $WebApp -AssignNewDatabaseId
				Set-SPContentDatabase $DBName -MaxSiteCount $DBLimit -WarningSiteCount 0
				write-host "End $(date)"
			}
			else
			{
				write-host "Aggiungo il db $DBName $(date) ..."
				Mount-SPContentDatabase $DBName -DatabaseServer $DBIstance -WebApplication $WebApp
				Set-SPContentDatabase $DBName -MaxSiteCount $DBLimit -WarningSiteCount 0
				write-host "End $(date)"
			}	
		}
}

Function DismountAllWeb()
{
	$webapps = Get-SPWebApplication
	
	foreach ($webapp in $webapps)
	{
		$wa = $webapp.Url
		get-spwebapplication -identity $wa | foreach { $SPContentDB = $_.ContentDatabases }
		$SPContentDB | ForEach { Dismount-SPContentDatabase $_ -confirm:$false }
	}
	
	
}

Function ExtractContentDBtoCSV($PathCSV)
{
	$webapps = Get-SPWebApplication

	$header = "WebApp;DBIstance;DBName;DBLimit;DBNewID"
	Add-Content -Value $header -Path $PathCSV
	
	foreach ($webapp in $webapps)
	{
		$url = $webapp.url
		$dbs = Get-SPContentDatabase -webapplication $url | select Name, Server, MaximumSiteCount, CurrentSiteCount
		
		foreach ($db in $dbs)
		{
			$dbServer = $db.Server
			$dbName = $db.Name
			$dbMaximumSiteCount = $db.MaximumSiteCount
			
				if ($dbMaximumSiteCount -gt 1000)
				{
					$dbMaximumSiteCount = $db.CurrentSiteCount
				}
			$dbNewID = "0"
			$value = "$url;$dbServer;$dbName;$dbMaximumSiteCount;$dbNewID"
			Add-Content -Value $value -Path $PathCSV
		}
	}
}

function Main($ScriptPath)
{
	
	if ((Get-PSSnapin | Where { $_.Name -eq "Microsoft.SharePoint.PowerShell" }) -eq $null)
	{
		Add-PSSnapin Microsoft.SharePoint.PowerShell;
	}
	
	do
	{
		$FarmName = Get-SPFarm
		$FarmName = $FarmName.Name
		$PathCSV = $ScriptPath + "\Export\" + $FarmName + ".csv"
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
		Write-Host "File CSV: " $PathCSV
		Write-Host "Select one of the available options:"
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
		Write-Host "1) Export to CSV Content DB"
		Write-Host "2) Dismount Content DB"
		Write-Host "3) Mount Content DB by CSV"
		Write-Host "0) Exit"
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
		$OpChoice = Read-Host
		Write-Host (FormatString -StringValue "" -MaxLength 79 -Left $false -FillChar '-')
		
		if ($OpChoice -eq "0")
		{
			Exit 0
		}
		elseif ($OpChoice -eq "1")
		{
			Write-Host "Exporting CSV, are you sure? (Y/N)"
			$OpConfirm = Read-Host
			if (($OpConfirm -ne "Y") -and ($OpConfirm -ne "y"))
			{
				continue
			}
			Write-Host -ForegroundColor Yellow "Starting Operation..."
			ExtractContentDBtoCSV -PathCSV $PathCSV
			Write-Host -ForegroundColor Green "Operation End";
		}
		elseif ($OpChoice -eq "2")
		{
			Write-Host "Dismount DB, are you sure? (Y/N)"
			$OpConfirm = Read-Host
			if (($OpConfirm -ne "Y") -and ($OpConfirm -ne "y"))
			{
				continue
			}
			Write-Host -ForegroundColor Yellow "Starting Operation..."
			DismountAllWeb
			Write-Host -ForegroundColor Green "Operation End";
		}
		elseif ($OpChoice -eq "3")
		{
			Write-Host "Mount DB, are you sure? (Y/N)"
			$OpConfirm = Read-Host
			if (($OpConfirm -ne "Y") -and ($OpConfirm -ne "y"))
			{
				continue
			}
			Write-Host -ForegroundColor Yellow "Starting Operation..."
			MountDBByCSV -PathCSV $PathCSV
			Write-Host -ForegroundColor Green "Operation End";
		}
	}
	while (1)
}


$ScriptPath = $myinvocation.mycommand.path | Split-Path
main -ScriptPath $ScriptPath