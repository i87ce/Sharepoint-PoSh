﻿if ((Get-PSSnapin "Microsoft.SharePoint.PowerShell" -ErrorAction SilentlyContinue) -eq $null) {
    Add-PSSnapin "Microsoft.SharePoint.PowerShell"
  }
Function Main
{
	Write-Host "How many CDB do you want to create?"
	[int]$NumCDB = Read-Host
	Write-Host "Start CDB Number"
	[int]$StartCDB = Read-Host

	[int]$MaxValue = $NumCDB + $StartCDB
	
	$tmpDBNamePath = "WSS_Content_people_"
	$DBServer = "EPTS-SHP13CSF-rdb1-pr.services.eni.intranet\SERVICES"
	$WebApp = "http://myprofile.eni.com"
	
	
	for([int]$i=$StartCDB; [int]$i -lt $MaxValue; [int]$i++)
	{
		$NewContentDBName =  $tmpDBNamePath + $i.ToString()
		
		Write-Host "Create ContentDB Name: "$NewContentDBName  -foreground "Yellow"
		New-SPContentDatabase $NewContentDBName -DatabaseServer $DBServer -WebApplication $WebApp
		Write-Host "ContentDB Creation Completed" -foreground "Green"
		Set-SPContentDatabase $NewContentDBName -MaxSiteCount 70 -WarningSiteCount 0
		Write-Host "Set Content DB Limit Completed" -foreground "Green"
	}
}

Main;

Remove-PsSnapin Microsoft.SharePoint.PowerShell


