﻿Add-PSSnapin Microsoft.SharePoint.Powershell

$upa = Get-SPServiceApplicationProxy | where { $_.TypeName -like "*Profile*" }
Update-SPRepopulateMicroblogLMTCache -ProfileServiceApplicationProxy  $upa

$site = Get-SPSite "http://myprofile.eni.com"
$ctx = Get-SPServiceContext $site
$upm = New-Object Microsoft.Office.Server.UserProfiles.UserProfileManager($ctx)
$profiles = $upm.GetEnumerator()
foreach ($profile in $profiles) {
    if($profile["SPS-PersonalSiteCapabilities"].Value -eq 14) {
	Write-Host $profile.AccountName
        Update-SPRepopulateMicroblogFeedCache -AccountName $profile.AccountName -ProfileServiceApplicationProxy  $upa
    }
}
use-CacheCluster
Get-AFCache | where { $_.CacheName -like "*Feed*"} | % { Get-AFCacheStatistics -CacheName $_.CacheName | Add-Member -MemberType NoteProperty -Name 'CacheName' -Value $_.CacheName -PassThru}
