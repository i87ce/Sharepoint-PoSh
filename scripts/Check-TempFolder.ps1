If (!(Test-Path $env:temp)) 
{
	Write-Host "Missing Temp Folder" -ForegroundColor 'Yellow'
	New-Item -Path $env:temp -type Directory
}
else 
{   
   Write-Host "Directory already exists!" -ForegroundColor 'Green'
}

$SessionId = [System.Diagnostics.Process]::GetCurrentProcess().SessionId
If (!(Test-Path $env:temp\$SessionId)) 
{
	Write-Host "Missing Remote Temp Folder" -ForegroundColor 'Yellow'
	New-Item -Path $env:temp\$SessionId -type Directory
}
else 
{   
   Write-Host "Directory already exists!" -ForegroundColor 'Green'
}