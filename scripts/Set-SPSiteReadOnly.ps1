function Set-LockState {
<#
.SYNOPSIS
Use this PowerShell script to set the Lock State of a SharePoint Web Application to Unlock, ReadOnly, NoAdditions or NoAccess.
.DESCRIPTION
This PowerShell script uses Set-SPSiteAdministration to set the Lock State of a SharePoint Web Application.
.EXAMPLE
C:\PS>.\SetAllSitesInAWebAppToReadOnly.ps1 -WebAppUrl http://intranet -LockState ReadOnly
This example sets all site collections in a web application at http://intranet to read-only.
.NOTES
This PowerShell script sets the Lock State of a SharePoint Web Application.
.LINK
 
http://www.iccblogs.com/blogs/rdennis
 
 
http://twitter.com/SharePointRyan
 
.INPUTS
None
.OUTPUTS
None
#>
 
## Input Parameters ##
[CmdletBinding()]
Param(
[string]$WebAppUrl=(Read-Host "Please enter a Web Application URL (Example: http://intranet)"),
[string]$LockState=(Read-Host "Please enter a Lock State (Examples: Unlock, ReadOnly)")
)
## Add SharePoint Snap-in ##
Add-PSSnapin Microsoft.SharePoint.PowerShell -erroraction SilentlyContinue
### No need to modify these ###
$WebApp = Get-SPWebApplication $WebAppUrl
$AllSites = $WebApp | Get-SPSite
 
############################# The Script - no need to modify this section #############################
## Start SP Assignment for proper disposal of variables and objects ##
Write-Host "Starting SP Assignment..." -ForegroundColor Green
Start-SPAssignment -Global
## Use a ForEach-Object loop and Set-SPSiteAdministration to set an entire web application ##
Write-Host "Setting $WebAppUrl to $lockState..." -ForegroundColor Yellow
$AllSites | ForEach-Object { Set-SPSiteAdministration -LockState $lockState -Identity $_.url }
## End SP Assignment for proper disposal of variables and objects ##
Write-Host "Disposing of SP Objects.." -ForegroundColor Green
Stop-SPAssignment -Global
## Tell us it's done ##
Write-Host "Finished!" -ForegroundColor Green
}