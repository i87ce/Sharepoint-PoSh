# Create New WebApplication
#
# 2015 Alessio Vigilante (alessio.vigilante@techroad.it)


# Output program information
Write-Host -foregroundcolor White ""
Write-Host -foregroundcolor White "Create New WebApplication"
Write-Host -foregroundcolor White ""

#**************************************************************************************
# Constants
#**************************************************************************************
Set-Variable Htdocs -option Constant -value "F:\Appl\"
Set-Variable IISLogs -option Constant -value "G:\Logs\IIS"

#**************************************************************************************
# Functions
#**************************************************************************************
#<summary>
# Loads the SharePoint Powershell Snapin.
#</summary>
Function Load-SharePoint-Powershell
{
	If ((Get-PsSnapin | ?{ $_.Name -eq "Microsoft.SharePoint.PowerShell" }) -eq $null)
	{
		Write-Host -ForegroundColor White " - Loading SharePoint Powershell Snapin"
		Add-PsSnapin Microsoft.SharePoint.PowerShell -ErrorAction Stop
	}
}

Function Load-IIS-Powershell
{
	Import-Module WebAdministration
}

#<summary>
# Check Managed Account and in case create it
#</summary>
#<param name="$Username">The name of the account.</param>
function CheckAndCreateManagedAccount($Username)
{
	Write-Host ""
	
	If ((Get-SPManagedAccount | Where { $_.UserName -eq $Username }) -eq $null)
	{
		Write-Host -foregroundcolor Gray "Account does not exists"
		#Write-Host -foregroundcolor DarkGray "Please, enter account password: "
		#$AccPassword = Read-Host
		$cred = Get-Credential
		
		#New-SPManagedAccount -Username $username -Password $password
		New-SPManagedAccount -Credential $cred
		Write-Host -foregroundcolor Gray "Account created"
	}
	else
	{
		Write-Host -foregroundcolor Gray "Account already exists"
	}
	
	Write-Host ""
}

#<summary>
# Main Function, create the web application
#</summary>
#<param name="$WebAppUrl">The web app url.</param>
#<param name="$UseSiteminder">Use Siteminder option</param>
function CreateWebApp($WebAppUrl, $UseSiteminder, $WebAppPort, $UseSSL, $DatabaseServer, $DatabaseName)
{	
	if (($UseSiteminder -eq "Y") -and ($UseSiteminder -eq "y"))
	{
			Write-Host -foregroundcolor DarkGray "enter web app pool account (example: eninet\adminsppoolawaret): "
			$WebAppUsername = Read-Host
			
			Write-Host -foregroundcolor Gray "Check Managed Account"
			CheckAndCreateManagedAccount -Username $WebAppUsername
			
			Write-Host -foregroundcolor Gray "Own Managed Account"
			$WebAppMAccount = Get-SPManagedAccount $WebAppUsername
			
			$WebAppName = $WebAppUrl + "_" + $WebAppPort
			$ExtWebAppUrl = $WebAppUrl
			$ExtWebAppName = $WebAppUrl + "_80"
			
			Write-Host -foregroundcolor Gray "Creating WebApp"

			$ap1 = New-SPAuthenticationProvider	
			
			New-SPWebApplication -Name $ExtWebAppName -Port 80 -HostHeader $ExtWebAppUrl -URL "http://$ExtWebAppUrl" -ApplicationPool $WebAppUrl -ApplicationPoolAccount $WebAppMAccount -AuthenticationProvider $ap1 -Path "$Htdocs\$ExtWebAppName\htdocs"

			Write-Host -foregroundcolor Gray "Extending WebApp"
			$ap = New-SPAuthenticationProvider
			
			if (($UseSSL -eq "Y") -and ($UseSSL -eq "y"))
			{
				Get-SPWebApplication -Identity $ExtWebAppName | New-SPWebApplicationExtension -Name $WebAppName -HostHeader $WebAppUrl -Zone Custom -URL "https://$WebAppUrl" -Port $WebAppPort -AuthenticationProvider $ap -SecureSocketsLayer -Path "$Htdocs\$WebAppName\htdocs"
			}
			else
			{
				Get-SPWebApplication -Identity $ExtWebAppName | New-SPWebApplicationExtension -Name $WebAppName -HostHeader $WebAppUrl -Zone Custom -URL "http://$WebAppUrl" -Port $WebAppPort -AuthenticationProvider $ap -Path "$Htdocs\$WebAppName\htdocs"
			}
			
			Write-Host -foregroundcolor Gray "WebApp Created"
			Write-Host -foregroundcolor Gray ""
			Write-Host -foregroundcolor Gray "Change IIS Logs Path"
			try {
				Write-Warning "Imposto path dei file di log .."
				Set-ItemProperty IIS:\Sites\${ExtWebAppName} -Name LogFile.directory -Value "$LogsDir\$ExtWebAppName" | Out-Null
			} 
			catch 
			{
				Write-Host -foreground red "Impossibile cambiare impostazioni Log"
			}
			try {
				Write-Warning "Imposto path dei file di log .."
				Set-ItemProperty IIS:\Sites\${WebAppName} -Name LogFile.directory -Value "$LogsDir\$WebAppName" | Out-Null
			} 
			catch 
			{
				Write-Host -foreground red "Impossibile cambiare impostazioni Log"
			}
			Write-Host -foregroundcolor Gray "Press any key to continue..."
			Read-Host
			.\Set-SPSiteMinder.ps1
			.\Set-PeoplePickerOneWay.ps1
	}
	else
	{
			Write-Host -foregroundcolor DarkGray "enter web app pool account (example: eninet\adminsppoolawaret): "
			$WebAppUsername = Read-Host
			
			Write-Host -foregroundcolor Gray "Check Managed Account"
			CheckAndCreateManagedAccount -Username $WebAppUsername
			
			Write-Host -foregroundcolor Gray "Own Managed Account"
			$WebAppMAccount = Get-SPManagedAccount $WebAppUsername
			
			$WebAppName = $WebAppUrl + "_" + $WebAppPort
			
			Write-Host -foregroundcolor Gray "Creating WebApp"
			$ap = New-SPAuthenticationProvider
			
			if (($UseSSL -eq "Y") -and ($UseSSL -eq "y"))
			{
				New-SPWebApplication -Name $WebAppName -Port $WebAppPort -HostHeader $WebAppUrl -URL "https://$WebAppUrl" -ApplicationPool $WebAppUrl -ApplicationPoolAccount $WebAppMAccount -AuthenticationProvider $ap -SecureSocketsLayer -Path "$Htdocs\$WebAppName\htdocs"
			}
			else
			{
				New-SPWebApplication -Name $WebAppName -Port $WebAppPort -HostHeader $WebAppUrl -URL "http://$WebAppUrl" -ApplicationPool $WebAppUrl -ApplicationPoolAccount $WebAppMAccount -AuthenticationProvider $ap -Path "$Htdocs\$WebAppName\htdocs"
			}
			
			Write-Host -foregroundcolor Gray ""
			Write-Host -foregroundcolor Gray "Change IIS Logs Path"
			try {
				Write-Warning "Imposto path dei file di log .."
				Set-ItemProperty IIS:\Sites\${WebAppName} -Name LogFile.directory -Value "$LogsDir\$WebAppName" | Out-Null
			} 
			catch 
			{
				Write-Host -foreground red "Impossibile cambiare impostazioni Log"
			}
			Write-Host -foregroundcolor Gray "Press any key to continue..."
			Read-Host
			.\Set-PeoplePickerOneWay.ps1
	}

}


#<summary>
# Collect Information and create web application
#</summary>
function CollectAndCreate
{
	Write-Host -foregroundcolor DarkGray "Please, Enter the url (without protocol and port): "
	$WebAppUrl = Read-Host
	Write-Host -foregroundcolor DarkGray ""
	
	Write-Host -foregroundcolor DarkGray "Enter Url Port: "
	$WebAppPort = Read-Host
	Write-Host -foregroundcolor DarkGray ""
	
	if ($WebAppPort -eq "443")
	{
		$UseSSL = "y"
	}
	elseif ($WebAppPort -eq "80")
	{
		$UseSSL = "n"
	}
	else
	{
		Write-Host -foregroundcolor DarkGray "Use SSL? (y/n): "
		$UseSSL = Read-Host
		Write-Host -foregroundcolor DarkGray ""
	}

	Write-Host -foregroundcolor DarkGray "Enter DB Connection String (leave empty for farm default): "
	$DatabaseServer = Read-Host
	Write-Host -foregroundcolor DarkGray ""
	
	$tmpUrl = $WebAppUrl.split(".")[0]
	$DatabaseName = "Wss_Content_$tmpUrl"	
	
	Write-Host -foregroundcolor DarkGray "Do you wish to use SiteMinder Integration? (y/n): "
	$UseSiteminder = Read-Host
	Write-Host -foregroundcolor DarkGray ""
	
	
	CreateWebApp -WebAppUrl $WebAppUrl -UseSiteminder $UseSiteminder -WebAppPort $WebAppPort -UseSSL $UseSSL -DatabaseServer $DatabaseServer -DatabaseName $DatabaseName
}


#**************************************************************************************
# Main script block
#**************************************************************************************

# Load SharePoint Powershell Snapin
Load-SharePoint-Powershell

# Load IIS Module
Load-IIS-Powershell

#Collect Information and start creating loop
do
{
	CollectAndCreate
	Write-Host -foregroundcolor DarkGray "Do you want to create another one? (y/N): "
	$OpContinue = Read-Host
	Write-Host -foregroundcolor DarkGray ""
}
while (($OpContinue -eq "Y") -and ($OpContinue -eq "y"))