# Output program information
Write-Host -foregroundcolor White ""
Write-Host -foregroundcolor White "Restart Sharepoint Farm"

#**************************************************************************************
# Constants
#**************************************************************************************
Set-Variable timerServiceName -option Constant -value "SPTimerV4"
Set-Variable adminServiceName -option Constant -value "SPAdminV4"
Set-Variable tracingServiceName -option Constant -value "SPTraceV4"

Set-Variable timerServiceInstanceName -option Constant -value "Microsoft SharePoint Foundation Timer"
Set-Variable adminServiceInstanceName -option Constant -value "Microsoft SharePoint Foundation Administration"
Set-Variable tracingServiceInstanceName -option Constant -value "Microsoft SharePoint Foundation Tracing"

#**************************************************************************************
# Restart Functions
#**************************************************************************************
function StopSharePointTimerServicesInFarm($farm)
{
	Write-Host ""
	
	# Iterate through each server in the farm, and each service in each server
	foreach ($server in $farm)
	{
		foreach ($instance in $server.ServiceInstances)
		{
			# If the server has the timer service then stop the service
			if ($instance.TypeName -eq $timerServiceInstanceName)
			{
				[string]$serverName = $server.Name
				
				Write-Host -foregroundcolor DarkGray -NoNewline "Stop '$timerServiceName' service on server: "
				Write-Host -foregroundcolor Gray $serverName
				
				$service = Get-WmiObject -ComputerName $serverName Win32_Service -Filter "Name='$timerServiceName'"
				sc.exe \\$serverName stop $timerServiceName > $null
				
				# Wait until this service has actually stopped
				WaitForServiceState $serverName $timerServiceName "Stopped"
				
				break;
			}
		}
	}
	
	Write-Host ""
}

function StartSharePointTimerServicesInFarm($farm)
{
	Write-Host ""
	
	# Iterate through each server in the farm, and each service in each server
	foreach ($server in $farm)
	{
		foreach ($instance in $server.ServiceInstances)
		{
			# If the server has the timer service then start the service
			if ($instance.TypeName -eq $timerServiceInstanceName)
			{
				[string]$serverName = $server.Name
				
				Write-Host -foregroundcolor DarkGray -NoNewline "Start '$timerServiceName' service on server: "
				Write-Host -foregroundcolor Gray $serverName
				
				$service = Get-WmiObject -ComputerName $serverName Win32_Service -Filter "Name='$timerServiceName'"
				sc.exe \\$serverName start $timerServiceName > $null
				
				WaitForServiceState $serverName $timerServiceName "Running"
				
				break;
			}
		}
	}
	
	Write-Host ""
}

function StopSharePointAdminServicesInFarm($farm)
{
	Write-Host ""
	
	# Iterate through each server in the farm, and each service in each server
	foreach ($server in $farm)
	{
		foreach ($instance in $server.ServiceInstances)
		{
			# If the server has the timer service then stop the service
			if ($instance.TypeName -eq $adminServiceInstanceName)
			{
				[string]$serverName = $server.Name
				
				Write-Host -foregroundcolor DarkGray -NoNewline "Stop '$adminServiceName' service on server: "
				Write-Host -foregroundcolor Gray $serverName
				
				$service = Get-WmiObject -ComputerName $serverName Win32_Service -Filter "Name='$adminServiceName'"
				sc.exe \\$serverName stop $adminServiceName  $null
				
				# Wait until this service has actually stopped
				WaitForServiceState $serverName $adminServiceName "Stopped"
				
				break;
			}
		}
	}
	
	Write-Host ""
}

function StartSharePointAdminServicesInFarm($farm)
{
	Write-Host ""
	
	# Iterate through each server in the farm, and each service in each server
	foreach ($server in $farm)
	{
		foreach ($instance in $server.ServiceInstances)
		{
			# If the server has the timer service then start the service
			if ($instance.TypeName -eq $adminServiceInstanceName)
			{
				[string]$serverName = $server.Name
				
				Write-Host -foregroundcolor DarkGray -NoNewline "Start '$adminServiceName' service on server: "
				Write-Host -foregroundcolor Gray $serverName
				
				$service = Get-WmiObject -ComputerName $serverName Win32_Service -Filter "Name='$adminServiceName'"
				sc.exe \\$serverName start $adminServiceName  $null
				
				WaitForServiceState $serverName $adminServiceName "Running"
				
				break;
			}
		}
	}
	
	Write-Host ""
}

function StopSharePointTracingServicesInFarm($farm)
{
	Write-Host ""
	
	# Iterate through each server in the farm, and each service in each server
	foreach ($server in $farm)
	{
		foreach ($instance in $server.ServiceInstances)
		{
			# If the server has the timer service then stop the service
			if ($instance.TypeName -eq $tracingServiceInstanceName)
			{
				[string]$serverName = $server.Name
				
				Write-Host -foregroundcolor DarkGray -NoNewline "Stop '$tracingServiceName' service on server: "
				Write-Host -foregroundcolor Gray $serverName
				
				$service = Get-WmiObject -ComputerName $serverName Win32_Service -Filter "Name='$tracingServiceName'"
				sc.exe \\$serverName stop $tracingServiceName  $null
				
				# Wait until this service has actually stopped
				WaitForServiceState $serverName $tracingServiceName "Stopped"
				
				break;
			}
		}
	}
	
	Write-Host ""
}

function StartSharePointTracingServicesInFarm($farm)
{
	Write-Host ""
	
	# Iterate through each server in the farm, and each service in each server
	foreach ($server in $farm)
	{
		foreach ($instance in $server.ServiceInstances)
		{
			# If the server has the timer service then start the service
			if ($instance.TypeName -eq $tracingServiceInstanceName)
			{
				[string]$serverName = $server.Name
				
				Write-Host -foregroundcolor DarkGray -NoNewline "Start '$tracingServiceName' service on server: "
				Write-Host -foregroundcolor Gray $serverName
				
				$service = Get-WmiObject -ComputerName $serverName Win32_Service -Filter "Name='$tracingServiceName'"
				sc.exe \\$serverName start $tracingServiceName  $null
				
				WaitForServiceState $serverName $tracingServiceName "Running"
				
				break;
			}
		}
	}
	
	Write-Host ""
}
#**************************************************************************************
# Main Functions
#**************************************************************************************
Function Load-SharePoint-Powershell
{
	If ((Get-PsSnapin | ?{ $_.Name -eq "Microsoft.SharePoint.PowerShell" }) -eq $null)
	{
		Write-Host -ForegroundColor White " - Loading SharePoint Powershell Snapin"
		Add-PsSnapin Microsoft.SharePoint.PowerShell -ErrorAction Stop
	}
}

function RestartIIS($farm)
{
	Write-Host ""
	
		# Iterate through each server in the farm, and each service in each server
		foreach ($server in $farm)
		{
			[string]$serverName = $server.Name
			
			if (($serverName -ne "T4SRV1015W") -and ($serverName -ne "T4SRV1015X") -and ($serverName -ne "T4SRV1015Z"))
			{
				Write-Host ""
				Write-Host -foregroundcolor DarkGray -NoNewline "IISReset on server: "
				Write-Host -foregroundcolor Gray $serverName
				iisreset $serverName
				Write-Host ""
			}
			else
			{
				Write-Host -foregroundcolor Gray "$serverName Skypped"
			}
		}
	

Write-Host ""
}

function WaitForServiceState([string]$serverName, [string]$serviceName, [string]$serviceState)
{
	Write-Host -foregroundcolor DarkGray -NoNewLine "Waiting for service '$serviceName' to change state to $serviceState on server $serverName"
	
	do
	{
		Start-Sleep 1
		Write-Host -foregroundcolor DarkGray -NoNewLine "."
		$service = Get-WmiObject -ComputerName $serverName Win32_Service -Filter "Name='$serviceName'"
	}
	while ($service.State -ne $serviceState)
	
	Write-Host -foregroundcolor DarkGray -NoNewLine " Service is "
	Write-Host -foregroundcolor Gray $serviceState
}

#**************************************************************************************
# Main script block
#**************************************************************************************

Write-Host "Restart IIS too? (y/N)"
$OpRestartIIS = Read-Host
Write-Host ""

# Load SharePoint Powershell Snapin
Load-SharePoint-Powershell

# Get the local farm instance
$farm = Get-SPServer | where { $_.Role -match "Application" }

if (($OpRestartIIS -eq "y") -or ($OpRestartIIS -eq "Y"))
{
	#restart iis service
	RestartIIS $farm
}

StopSharePointTimerServicesInFarm $farm
StartSharePointTimerServicesInFarm $farm
StopSharePointAdminServicesInFarm $farm
StartSharePointAdminServicesInFarm $farm
StopSharePointTracingServicesInFarm $farm
StartSharePointTracingServicesInFarm $farm