﻿param (	[string] $SHPEdition = $(Read-Host -Prompt "Please enter the sharepoint edition (2010/2013)"),
		[string] $Disk = $(Read-Host -Prompt "Please enter the sw disk"))

# Import Required Modules
Import-Module BitsTransfer

$DownloadUrls = New-Object -typeName System.Collections.Arraylist
if ($SHPEdition -eq "2013")
{
	# Specify download url's for SharePoint 2013
	$DownloadUrls.Add("\\ennf1002.eni.pri\ASA_PCK_COLLABORATION-MOET\Unattended-Sharepoint\Source\2013")
}
elseif ($SHPEdition -eq "2010")
{
	# Specify download url's for SharePoint 2010
	$DownloadUrls.Add("\\ennf1002.eni.pri\ASA_PCK_COLLABORATION-MOET\Unattended-Sharepoint\Source\2010")
}

function CopyFiles-Bits
{
    # Set Parameters
    Param (
        [Parameter(Mandatory=$true)]
        [String]$Source,
		[Parameter(Mandatory=$false)]
        [Switch]$CreateParent=$false,
        [Parameter(Mandatory=$true)]
        [String]$Destination
    )
	
	$FileName = $Source.Split('\')[-1]
	Write-Host "Downloading: $FileName"
	
	
	# Read all directories located in the sorce directory
	$folders = Get-ChildItem -Path $source -Recurse | ?{$_.PSisContainer}
	
	Try
	{
		if ($CreateParent)
		{
			$exists = Test-Path $Destination\$FileName
			if ($exists -eq $false) 
			{
				New-Item $Destination\$FileName -ItemType Directory
			}			
			$Destination = "$Destination\$FileName"
		}
		
		Start-BitsTransfer -Source $Source\*.* -Destination $Destination -DisplayName "Downloading `'$FileName`' to $Destination" -Priority High -Description "From $Source..." -ErrorVariable err
		foreach ($i in $folders)
		{
			$i = $i.FullName.Remove(0,$source.Length+1)
			
			$exists = Test-Path $Destination\$i
			if ($exists -eq $false) 
			{
				New-Item $Destination\$i -ItemType Directory
			}
			Start-BitsTransfer -Source $Source\$i\*.* -Destination $Destination\$i -DisplayName "Downloading `'$FileName`' to $Destination" -Priority High -Description "From $Source..." -ErrorVariable err
		}
		If ($err) { Throw "" }
		
		#Get-ChildItem -Path $source -Recurse | ?{$_.PSisContainer} | foreach {$spath = $_.FullName.Remove(0,$source.Length+1); Start-BitsTransfer -Source $source\$spath\*.* -Destination $Destination\$spath } 
	

	}
	Catch
	{
		$ReturnCode = -1
		Write-Warning " - An error occurred downloading `'$FileName`'"
		Write-Error $_
		break
	}
}

function DownLoadPreRequisites()
{
	
	Write-Host ""
	Write-Host "====================================================================="
	Write-Host "      Downloading SharePoint Unattended Please wait..."
	Write-Host "====================================================================="
	
	$ReturnCode = 0
	
	CopyFiles-Bits -Source "\\ennf1002.eni.pri\ASA_PCK_COLLABORATION-MOET\Unattended-Sharepoint\Source\sw" -Destination $tmpPath
	
	foreach ($DownLoadUrl in $DownloadUrls)
	{
		
		CopyFiles-Bits -Source $DownLoadUrl -Destination $tmpPath -CreateParent
	}
	Write-Host " - Done downloading setup required for SharePoint"
	
	return $ReturnCode
}

function CheckProvidedDownloadPath()
{
	$ReturnCode = 0
	Try
	{
		# Check if destination path exists
		If (Test-Path "$Disk\sw")
		{
			# Remove trailing slash if it is present
			$script:tmpPath = Join-Path $Disk "sw"
			$script:tmpPath = $tmpPath.TrimEnd('\')
			$ReturnCode = 0
		}
		Else
		{
			$ReturnCode = -1
			Write-Host ""
			Write-Warning "Your specified download path does not exist."
			New-Item -ItemType directory -Path "$Disk\sw"
			Write-Host "Created..."
			Write-Host ""
			CheckProvidedDownloadPath
		}
	}
	Catch
	{
		$ReturnCode = -1
		Write-Warning "An error has occurred when checking your specified download path"
		Write-Error $_
		break
	}
	
	return $ReturnCode
	
}

function Main()
{

	$Disk = $Disk + ":\"
	
	$rc = 0
	$rc = CheckProvidedDownloadPath
	
	# Download Pre-Reqs
	if ($rc -ne -1)
	{
		$rc = DownLoadPreRequisites
	}
	if ($rc -ne -1)
	{
		Write-Host ""
		Write-Host "Script execution is now complete!"
		Write-Host ""
		&"$tmpPath\Unattended-Setup_ConfigOnly.ps1 -SHPEdition $SHPEdition -Disk $Disk"
	}
}

Main