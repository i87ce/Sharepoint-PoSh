function InstallSharepointPatch($installPath, $installName)
{
	Write-Host "Installing SharePoint Patch $installName..." -ForegroundColor Green
	
	$path = Join-Path $installPath $installName
	$args = "/passive"
	
	$sharepoint = (Start-Process -Wait -PassThru $path -ArgumentList $args)
	
	switch ($sharepoint.ExitCode)
	{
		0 {
			Write-Host "SharePoint successfully installed" -ForegroundColor Green
		}
		17021 {
			Write-Host "Missing Temp Folder" -ForegroundColor 'Yellow'
			New-Item $env:temp -type directory
			InstallSharepointPatch -installName $installName -installPath $installPath
		}
		17022 {
			Write-Host "Server needs to be rebooted" -ForegroundColor 'Yellow'
			StartService
			Restart-Computer -Confirm
		}
		default
		{
			Write-Host "An error has occured. Code: " $sharepoint.ExitCode -ForegroundColor Red
		}
	}
	
	return $sharepoint.ExitCode
}

function Main($ScriptPath)
{
	if ((Get-PSSnapin | Where { $_.Name -eq "Microsoft.SharePoint.PowerShell" }) -eq $null)
	{
		Add-PSSnapin Microsoft.SharePoint.PowerShell;
	}
	$Version = GetSharepointVersion
	Write-Host "Sharepoint Version: $Version"
	$starttime = Get-Date
	Write-Host "Please, enter patch path" -ForegroundColor 'Green'
	$PatchPath = Read-Host

	$ListaPatch = Get-ChildItem -Path $PatchPath -Filter "*.exe" -Recurse | Sort-Object Directory, Name
    
    	if ($Version -eq "14")
    	{
        	$NumPatchFound = ($ListaPatch | Measure-Object).count
		
    	}
    	elseif ($Version -eq "15")
    	{
        	$NumPatchFound = $ListaPatch.count
    	}

	Write-Host "Found $NumPatchFound Patch"
	
	if ($NumPatchFound -gt 0)
	{
		StopSearchSearvice
		StopOtherService
		$Continue = $true
		
		foreach ($Patch in $ListaPatch)
		{
			if ($Continue -eq $true)
			{				
				Write-Host -ForegroundColor 'Yellow' "Install patch "$Patch.DirectoryName\$Patch"? (y/n)"
				$OpChoiseInstall = Read-Host
				if (($OpChoiseInstall -eq "y") -or ($OpChoiseInstall -eq "Y"))
				{
					InstallSharepointPatch -installPath $Patch.DirectoryName -installName $Patch.Name
				}
				else
				{
					Write-Host -ForegroundColor 'Cyan' "Patch "$Patch.DirectoryName\$Patch" Skypped"
				}
				
				
				
				Write-Host -ForegroundColor 'Yellow' "Continue? (y/n)"
				$OpChoiseContinue = Read-Host
				if (($OpChoiseContinue -eq "y") -or ($OpChoiseContinue -eq "Y"))
				{
					$Continue = $true
				}
				else
				{
					$Continue = $false
				}
			}
		}
		StartService
	}
	else
	{
		Write-Host "Unable to retrieve the file.  Exiting Script" -ForegroundColor Red 
	}
	$finishtime = get-date 
	Write-Host "Script Duration" -foregroundcolor yellow 
	Write-Host "Started: " $starttime -foregroundcolor yellow 
	Write-Host "Finished: " $finishtime -foregroundcolor yellow 
	Write-Host "Script Complete" 
}


function GetSharepointVersion()
{
	$SPVersion = ""
	$SPVersion = (Get-PSSnapin microsoft.sharepoint.powershell).Version.Major
	return $SPVersion
}

########################
##Stop Search Services## 
######################## 
function StopSearchSearvice()
{
	$srchctr = 1
	$srch4srvctr = 1
	$srch5srvctr = 1
	
	if ($Version -eq "15")
	{
		$srv4Name = "OSearch15"
		$srv5Name = "SPSearchHostController"
	}
	elseif ($Version -eq "14")
	{
		$srv4Name = "OSearch14"
		$srv5Name = ""
	}
	
	$srv4 = get-service $srv4Name
	
	
	
	If (($srv4.status -eq "Running") -or ($srv5.status -eq "Running"))
	{
		Write-Host "Choose 1 to Pause Search Service Application" -ForegroundColor Cyan
		Write-Host "Choose 2 to leave Search Service Application running" -ForegroundColor Cyan
		$searchappresult = Read-Host "Press 1 or 2 and hit enter"
		Write-Host
		
		
		if ($searchappresult -eq 1)
		{
			$srchctr = 2
			Write-Host "Pausing the Search Service Application" -foregroundcolor yellow
			Write-Host "This could take a few minutes" -ForegroundColor Yellow
			$ssa = get-spenterprisesearchserviceapplication
			$ssa.pause()
		}
		
		
		elseif ($searchappresult -eq 2)
		{
			Write-Host "Continuing without pausing the Search Service Application"
		}
		else
		{
			Write-Host "Run the script again and choose option 1 or 2" -ForegroundColor Red
			Write-Host "Exiting Script" -ForegroundColor Red
			Return
		}
	}
	
	Write-Host "Stopping Search Services if they are running" -foregroundcolor yellow
	if ($srv4.status -eq "Running")
	{
		$srch4srvctr = 2
		set-service -Name $srv4Name -startuptype Disabled
		$srv4.stop()
	}
	
	if ($Version -eq "15")
	{
		$srv5 = get-service $srv5Name
		if ($srv5.status -eq "Running")
		{
			$srch5srvctr = 2
			Set-service $srv5Name -startuptype Disabled
			$srv5.stop()
		}
		
		do
		{
			$srv6 = get-service $srv5Name
			if ($srv6.status -eq "Stopped")
			{
				$yes = 1
			}
			Start-Sleep -seconds 10
		}
		until ($yes -eq 1)
	}
	Write-Host "Search Services are stopped" -foregroundcolor Green 
	Write-Host
}

####################### 
##Stop Other Services## 
####################### 
function StopOtherService()
{
	Set-Service -Name "IISADMIN" -startuptype Disabled 
	Set-Service -Name "SPTimerV4" -startuptype Disabled 
	Write-Host "Gracefully stopping IIS W3WP Processes" -foregroundcolor yellow 
	Write-Host 
	iisreset -stop -noforce 
	Write-Host "Stopping Services" -foregroundcolor yellow 
	Write-Host

	$srv2 = get-service "SPTimerV4" 
	  if($srv2.status -eq "Running") 
	  {$srv2.stop()}

	Write-Host "Services are Stopped" -ForegroundColor Green 
	Write-Host 
	Write-Host
}
 
################## 
##Start Services## 
################## 
function StartService()
{
	Write-Host "Starting Services Backup" -foregroundcolor yellow 
	Set-Service -Name "SPTimerV4" -startuptype Automatic 
	Set-Service -Name "IISADMIN" -startuptype Automatic

	##Grabbing local server and starting services## 
	$servername = hostname 
	#$server = get-spserver $servername

	$srv2 = get-service "SPTimerV4" 
	$srv2.start() 
	$srv3 = get-service "IISADMIN"
	$srv3.start()
	
	if ($Version -eq "15")
	{
		$srv4Name = "OSearch15"
		$srv5Name = "SPSearchHostController"
	}
	elseif ($Version -eq "14")
	{
		$srv4Name = "OSearch14"
		$srv5Name = ""
	}
	
	
	$srv4 = get-service $srv4Name
	

	###Ensuring Search Services were stopped by script before Starting" 
	if($srch4srvctr -eq 2) 
	{ 
		set-service -Name "OSearch15" -startuptype Automatic 
		$srv4.start() 
	} 
	if (($srch5srvctr -eq 2) -and ($Version -eq "15"))
	{
		Set-service $srv5Name -startuptype Automatic
		$srv5 = get-service $srv5Name
		$srv5.start() 
	}

	###Resuming Search Service Application if paused### 
	if($srchctr -eq 2) 
	{ 
		Write-Host "Resuming the Search Service Application" -foregroundcolor yellow 
		$ssa = get-spenterprisesearchserviceapplication 
		$ssa.resume() 
	}

	Write-Host "Services are Started" -foregroundcolor green 
}
$ScriptPath = $myinvocation.mycommand.path | Split-Path
main -ScriptPath $ScriptPath