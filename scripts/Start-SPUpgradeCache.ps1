#-----------------------------------------------------------------------------
 # Name:            Update-AppFabric.ps1 
 # Description:      This script will patch and enable Garbage Collection in AppFabric
 # Usage:         Run script, make sure that the AppFabric CU is in the same directory
 #   as the script and that there are no other exes in the same directory.
 #   Do not rename the AppFabric Cumulative Update!
 #-----------------------------------------------------------------------------
param (
   [string]$patchpath
)

if ([string]::IsNullOrEmpty($patchpath))
{
	$patchfile = Get-ChildItem | where {$_.Name.ToLower().StartsWith("appfabric1.1") -and $_.Extension -eq ".exe"}
}
else
{
	$patchfile = Get-ChildItem -path $patchpath | where {$_.Name.ToLower().StartsWith("appfabric1.1") -and $_.Extension -eq ".exe"}
}

if($patchfile -eq $null) 
{ 
  Write-Host "Unable to retrieve the file.  Exiting Script" -ForegroundColor Red;
  Return;
}
## Add SharePoint Snap-in ##
Add-PSSnapin Microsoft.SharePoint.PowerShell -erroraction SilentlyContinue
 Write-Host "Stopping AppFabric" -ForegroundColor Magenta;
 Stop-SPDistributedCacheServiceInstance -Graceful
 Write-Host "Stopping AppFabric complete" -ForegroundColor green;

 Write-Host "Patching now keep this PowerShell window open" -ForegroundColor Magenta;
 Start-Process -FilePath $patchfile.FullName -ArgumentList "/passive" -Wait;
 Write-Host "Patch installation complete" -foregroundcolor green;

 Write-Host "Updating AppFabric config" -ForegroundColor Magenta;
 $location = "C:\Program Files\AppFabric 1.1 for Windows Server\DistributedCacheService.exe.config"
 $xml = [xml](get-content $location);

 if ($xml.configuration.appSettings.add -eq $null) {
  $appsettings = $xml.CreateElement("appSettings");
  $add = $xml.CreateElement("add");
  $key = $xml.CreateAttribute("key");
  $key.InnerText = "backgroundGC";
  $add.Attributes.Append($key) | out-null;
  $value = $xml.CreateAttribute("value");
  $value.InnerText = "true";
  $add.Attributes.Append($value) | out-null;
  $appsettings.AppendChild($add) | out-null;
  $configsections = $xml.configuration.configSections;
  $xml.configuration.InsertAfter($appsettings,$configsections) | out-null;
  $xml.save($location);
 }
 Write-Host "Updating AppFabric config complete" -ForegroundColor green;

 Write-Host "Starting AppFabric" -ForegroundColor Magenta;
 $instance = Get-SPServiceInstance | ? {$_.TypeName -eq "Distributed Cache" -and $_.Server.Name -eq $env:computername};
 $instance.Provision();
 Write-Host "Starting AppFabric complete" -ForegroundColor Magenta;