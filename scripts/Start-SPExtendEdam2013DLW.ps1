if ((Get-PSSnapin "Microsoft.SharePoint.PowerShell" -ErrorAction SilentlyContinue) -eq $null) {
    Add-PSSnapin "Microsoft.SharePoint.PowerShell"
  }

#Get available quota templates in farm
$Templates = [Microsoft.SharePoint.Administration.SPWebService]::ContentService.QuotaTemplates

Function GetActualQuotaTemplate ([string]$Url)
{
	$value = "" | Select-Object -Property Name,ID
	$site = Get-SPSite -Identity $Url
	$currentQuotaID = $site.Quota.QuotaID
	$quotaFound = $false

	#Loop through each quota template in the farm looking for a mact in IDs
	foreach($Template in $Templates)
	{
		#If there is a macth found, break out of the loop and move on to the next site
		if($currentQuotaID -eq $Template.QuotaID)
		{
			#Write-Host ("Quota found for " + $site.URL + ". QuotaID: " + $currentQuotaID + ". Quota Name: " + $Template.Name)
			$value.ID = $Template.QuotaID
			$value.Name = $Template.Name
			$quotaFound = $true
			break 
		}
	}
	#If no match is found, output it
	if($quotaFound -eq $false)
	{
		#Write-Host ("No quota found for " + $site.URL + ". QuotaID: " + $currentQuotaID)
		$value.ID = ""
		$value.Name = ""
	}
	$site.Dispose()
	return $value
}

Function GetNextTemplateName ([string]$ActualTemplateName)
{
	#Possible Value Template
	#34642 - DLW_Minimal
	#17988 - DLW_Standard
	#34209 - DLW_Large
	#46915 - DLW_XL
	#62727 - DLW_XXL
	$value = "" | Select-Object -Property Name,DBPrefix,Limit
	if ($ActualTemplateName -eq "DLW_Minimal")
	{
		$value.Name = "DLW_Standard"
		$value.DBPrefix = "WSS_Content_Edam_M*"
		$value.Limit = 10
	}
	if ($ActualTemplateName -eq "DLW_Standard")
	{
		$value.Name = "DLW_Large"
		$value.DBPrefix = "WSS_Content_Edam_L*"
		$value.Limit = 4
	}
	if ($ActualTemplateName -eq "DLW_Large")
	{
		$value.Name = "DLW_XL"
		$value.DBPrefix = "WSS_Content_Edam_XL*"
		$value.Limit = 2
	}
	if ($ActualTemplateName -eq "DLW_XL")
	{
		$value.Name = "DLW_XXL"
		$value.DBPrefix = "WSS_Content_Edam_XXL*"
		$value.Limit = 1
	}
	return $value
}

Function Send-Email {
    [CmdletBinding()]
    Param(
        [Parameter(Position=0,Mandatory=$true)]
        [String]$Subject="",
        [Parameter(Position=1,Mandatory=$true)]
        [Object]$To=@(),
        [Parameter(Position=2,Mandatory=$false)]
        [Object]$Cc="",
        [Parameter(Position=3,Mandatory=$false)]
        [String]$Body="",
        [Parameter(Position=4,Mandatory=$false)]
        [String]$from="UpgradeEdamQuota@eni.com",
        [Parameter(Position=5,Mandatory=$false)]
        [String]$domain="relay.services.eni.intranet"
    )
    Process {
        $mail = New-Object System.Net.Mail.MailMessage
        for($i=0; $i -lt $To.Length; $i++) {
            $mail.To.Add($To[$i]);
        }
		for($i=0; $i -lt $Cc.Length; $i++) {
            $mail.Cc.Add($Cc[$i]);
        }
        $mail.From = New-Object System.Net.Mail.MailAddress($from)
        $mail.Subject = $Subject
		$mail.IsBodyHtml = 1
        $mail.Body = $Body
        $smtp = New-Object System.Net.Mail.SmtpClient($domain)
        $smtp.Send($mail)
        $mail.Dispose()
    }
}

Function Main
{
	$SCPath = Read-Host "Enter DLW Name"
	$SiteCollection = "http://edam2013.eni.com/dlw/" + $SCPath
	$Confirm = Read-Host "Confirm url "$SiteCollection"? (Y/N)"
	if ($Confirm -eq "Y")
	{
		$ActualTemplate = GetActualQuotaTemplate($SiteCollection)

		if ([string]::IsNullOrEmpty($ActualTemplate.Name))
		{
			Write-Host ("No Template for site: " + $SiteCollection) -foreground "Red";
		}
		else
		{
			Write-Host ("Actual Template: " + $ActualTemplate.Name) -foreground "Green";
			$NextTemplate = GetNextTemplateName($ActualTemplate.Name)
			Write-Host ("New Template: " + $NextTemplate.Name) -foreground "Green";
			$ActualContentDB = Get-SPSite -Identity $SiteCollection
			$NextContentDB = Get-SPContentDatabase -WebApplication http://edam2013.eni.com | Where-Object {$_.name -like $NextTemplate.DBPrefix -and $_.CurrentSiteCount -lt $_.MaximumSiteCount} | select -last 1
			if ([string]::IsNullOrEmpty($NextContentDB.Name))
			{
				&"Add-Edam2013CDB.ps1"
				$NextContentDB = Get-SPContentDatabase -WebApplication http://edam2013.eni.com | Where-Object {$_.name -like $NextTemplate.DBPrefix -and $_.CurrentSiteCount -lt $_.MaximumSiteCount} | select -last 1
			}
			$DisplayNextContentDBName = ($NextContentDB.Name -split '=')[0]
			Write-Host "New Content Database: $DisplayNextContentDBName" -foreground "Green";
			$Confirm = Read-Host "Confirm Move "$SiteCollection" to DB "$DisplayNextContentDBName"? (Y/N)"
			if ($Confirm -eq "Y")
			{
				Write-Host "Lock DLW" -foreground "Yellow"
				Set-SPSite -Identity $SiteCollection -LockState "ReadOnly"
				Write-Host "DLW Locked" -foreground "Green"
				Write-Host "Move DLW" -foreground "Yellow"
				Move-SPSite $SiteCollection -DestinationDatabase $NextContentDB -Confirm:$false
				Write-Host "DLW Moved Complete" -foreground "Green"
				Write-Host "Unlock DLW" -foreground "Yellow"
				Set-SPSite -Identity $SiteCollection -LockState "Unlock"
				Write-Host "DLW Unlocked" -foreground "Green"
			}
			else
			{
				Write-Host "Move DLW Skipped" -foreground "Red"
			}
			$Confirm = Read-Host "Confirm Apply new Template "$NextTemplate.Name" to DLW "$SiteCollection"? (Y/N)"
			if ($Confirm -eq "Y")
			{
				Write-Host "Apply new template" -foreground "Yellow"
				#AssignQuotaTemplate($NextTemplate.Name,$SiteCollection)
				$contentService = [Microsoft.SharePoint.Administration.SPWebService]::ContentService
				$quotaTemplate = $contentService.QuotaTemplates[$NextTemplate.Name]
				Set-SPSite -Identity $SiteCollection -QuotaTemplate $quotaTemplate
				Write-Host "New template applied" -foreground "Green"
			}
			else
			{
				Write-Host "New quota template skipped" -foreground "Red"
			}
			
			$NewTemplate = GetActualQuotaTemplate($SiteCollection)
			$NewContentDB = Get-SPSite -Identity $SiteCollection
			Write-Host ("Old Template: " + $ActualTemplate.Name + " Old content DB: " + ($ActualContentDB.ContentDatabase -split '=')[1]) -foreground "Magenta";
			Write-Host ("Actual Template: " + $NewTemplate.Name + " New content DB: " + ($NewContentDB.ContentDatabase -split '=')[1]) -foreground "Magenta";
			$Confirm = Read-Host "Send Mail? (Y/N)"
			if ($Confirm -eq "Y")
			{
				$Oggetto = "Upgrade Edam2013 Quota: " + $SCPath
				$Corpo = " Upgrade Quota DLW: " + $SiteCollection + "`r`n"
				$Corpo += " Old Template: " + $ActualTemplate.Name + " Old content DB: " + ($ActualContentDB.ContentDatabase -split '=')[1] + "`r`n"
				$Corpo += " Actual Template: " + $NewTemplate.Name + " New content DB: " + ($NewContentDB.ContentDatabase -split '=')[1] + "`r`n"
				
				$To = @("ASA_PCK_Collaboration@eni.com")
				$Cc = @("")
				
				$Confirm = Read-Host "Send Mail to AM too? (Y/N)"
				if ($Confirm -eq "Y")
				{
					$Cc = @("edamhelp@eni.com")
				}
				Send-Email -Subject $Oggetto -Body $Corpo -To $To -Cc $Cc
			}
			
			$Confirm = Read-Host "Confirm IIS Reset? (Y/N)"
			if ($Confirm -eq "Y")
			{
				Write-Host "Start Farm IISReset" -foreground "Yellow"
				& "V:\scripts\Restart-SharepointFarm.ps1"
				Write-Host "IIS Reset Complete" -foreground "Green"
			}
			
		}		
	}
	Write-Host "Bye!" -foreground "Green"
}
Main;
Remove-PsSnapin Microsoft.SharePoint.PowerShell


