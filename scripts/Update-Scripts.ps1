function CopyFiles-Bits
{
    # Set Parameters
    Param (
        [Parameter(Mandatory=$true)]
        [String]$Source,
		[Parameter(Mandatory=$false)]
        [Switch]$CreateParent=$false,
        [Parameter(Mandatory=$true)]
        [String]$Destination
    )
	
	$FileName = $Source.Split('\')[-1]
	Write-Host "Downloading: $FileName"
	
	
	# Read all directories located in the sorce directory
	$folders = Get-ChildItem -Path $source -Recurse | ?{$_.PSisContainer}
	
	Try
	{
		if ($CreateParent)
		{
			$exists = Test-Path $Destination\$FileName
			if ($exists -eq $false) 
			{
				New-Item $Destination\$FileName -ItemType Directory
			}			
			$Destination = "$Destination\$FileName"
		}
		
		Start-BitsTransfer -Source $Source\*.* -Destination $Destination -DisplayName "Downloading `'$FileName`' to $Destination" -Priority High -Description "From $Source..." -ErrorVariable err
		foreach ($i in $folders)
		{
			$i = $i.FullName.Remove(0,$source.Length+1)
			
			$exists = Test-Path $Destination\$i
			if ($exists -eq $false) 
			{
				New-Item $Destination\$i -ItemType Directory
			}
			Start-BitsTransfer -Source $Source\$i\*.* -Destination $Destination\$i -DisplayName "Downloading `'$FileName`' to $Destination" -Priority High -Description "From $Source..." -ErrorVariable err
		}
		If ($err) { Throw "" }

	}
	Catch
	{
		$ReturnCode = -1
		Write-Warning " - An error occurred downloading `'$FileName`'"
		Write-Error $_
		break
	}
}

CopyFiles-Bits -Source "\\ennf1002.eni.pri\ASA_PCK_COLLABORATION-MOET\Script" -Destination "V:\"